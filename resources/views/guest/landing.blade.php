@extends('layouts.guest_main')

@section('title')
    Bioskop
@endsection

@section('content')
    <style>
        body{
            overflow: hidden;
        }
        .bg{
            background-image: url("{{ asset('/img/landing-bg.png') }}");
            background-size: cover;
            background-repeat: no-repeat;
        }
        .titlee{
            font-size: 4em; 
            color: #ffffff;
        }

/*screen-sm*/
@media (min-width: 768px) and (max-width: 992px) { 
    .right{text-align: right;}
  .left{text-align: left;}
}
        /*screen-md*/
@media (min-width: 992px) and (max-width: 1200px) { 
  .right{text-align: right;}
  .left{text-align: left;}
}

/*screen-xs*/
@media (max-width: 768px) { 
    .right{text-align: center;}
  .left{text-align: center;}
}

/*screen-lg corresponds with col-lg*/
@media (min-width: 1200px) {  
    .right{text-align: right;}
  .left{text-align: left;}
}
    </style>
    <div class="wrapper">
        <div class="section bg">
            <div class="page-header">
                <div class="container mt-5 animate__animated animate__fadeIn" style="animation-duration: 1s;">
                    <div class="row text-center">
                        <div class="col">
                            <h5 class="titlee"><b>EDM •</b> Cinema</h5>
                        </div>
                    </div>
                    <div class="row">
                        <!-- <div class="col-4"></div> -->
                        <!-- <div class="col-md-2 col-sm-4 text-center"> -->
                        <div class="col-md-6 right">
                            <button class="login btn btn-primary">Log In</button>
                        </div>
                        
                        <!-- <div class="col-md-2 col-sm-4 text-center"> -->
                        <div class="col-md-6 left">
                            <button class="reg btn btn-success">Sign Up</button>
                        </div>
                        <!-- <div class="col-4"></div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(".login").click(function(){
            window.location.href = '/bioskop/public/login'
        })
        $(".reg").click(function(){
            window.location.href = '/bioskop/public/register'
        })
    </script>
@endsection