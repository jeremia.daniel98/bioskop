@extends('layouts.guest_main')

@section('title')
    Login
@endsection

@section('content')
    <style>
        body{
            background-image: url("{{ asset('/img/login-bg.png') }}");
            background-size: cover;
            background-repeat: no-repeat;
        }

/*screen-xs*/
@media (min-width: 0px) and (max-width: 768px) { 
  .cardeu{margin-top: 3em;}
  .card-img{
      width: 23em !important;
  }
}
/*screen-sm*/
@media (min-width: 768px) and (max-width: 992px) { 
  .cardeu{margin-top: 5em;}
}
        /*screen-md*/
@media (min-width: 992px) and (max-width: 1200px) { 
  .cardeu{margin-top: 10em;}
}
/*screen-lg corresponds with col-lg*/
@media (min-width: 1200px) {  
  .cardeu{margin-top: 10em;}
}
    </style>
    <div class="wrapper bg">
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div id="bg" class="col-lg-5 col-md-6 col-sm-8 col-xs-8 offset-xs-0 offset-lg-1 offset-sm-2 offset-md-3 cardeu">
                        <div class="card card-register animate__animated animate__fadeIn" style="box-shadow: 5px 5px 20px black;">
                            <div class="card-header">
                            <img class="card-img" src="{{ asset('/img/square-purple-1.png') }}">
                                <h2 class="d-none d-sm-block card-title text-white pr-3 pt-3">Log In</h2>
                                <h2 class="d-block d-sm-none card-title text-white smaller" style="font-size: 4.5em;">Log In</h2>
                            </div>
                            <div class="card-body">
                                <form action="#" class="form">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="tim-icons icon-single-02"></i>
                                            </div>
                                        </div>
                                        <input class="form-control" type="text" name="username" id="username" placeholder="Username">
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="tim-icons icon-lock-circle"></i>
                                            </div>
                                        </div>
                                        <input class="form-control" type="password" name="password" id="password" placeholder="Password">
                                    </div>
                                    <div class="card-footer text-right">
                                        <button type="button" id="login" class="btn btn-primary btn-round btn-lg">Login</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    



    
@endsection

@section('script')
    <script>

        function login() {
            $.ajax({
                method: "POST",
                url: "login",
                data: {
                    'username': $("#username").val(),
                    'password': $("#password").val(),
                    '_token': _token
                }
            }).done(function(data){
                if(data['err_code'] == 0){
                    window.location.href = data['msg'];
                } else {
                    $.notify({
                        message: data["msg"]
                    }, {
                        type: 'danger',
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        z_index: 9999
                    });
                }
            })
        }

        $(document).ready(function() {
            $("#login").click(function() {
                login();
            });

            $("#password").keydown(function(e) {
                if(e.which == 13){
                    login();
                }
            })

            $("#username").keydown(function(e) {
                if(e.which == 13){
                    login();
                }
            })
        })
    </script>
@endsection