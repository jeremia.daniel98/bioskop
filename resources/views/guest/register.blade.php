@extends('layouts.guest_main')

@section('title')
    Register
@endsection

@section('content')
    <style>
        body{
            background-image: url("{{ asset('/img/register-bg.png') }}");
            background-size: cover;
            background-repeat: no-repeat;
        }
/*screen-xs*/
@media (min-width: 0px) and (max-width: 768px) { 
  .cardeu{margin-top: 3em;}
}
/*screen-sm*/
@media (min-width: 768px) and (max-width: 992px) { 
  .cardeu{margin-top: 5em;}
}
        /*screen-md*/
@media (min-width: 992px) and (max-width: 1200px) { 
  .cardeu{margin-top: 10em;}
}
/*screen-lg corresponds with col-lg*/
@media (min-width: 1200px) {  
  .cardeu{margin-top: 10em;}
}
    </style>
    <div class="wrapper">
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-6 col-sm-8 col-xs-8 offset-xs-0 offset-lg-6 offset-sm-2 offset-md-3 cardeu">
                        <div class="card card-register animate__animated animate__fadeIn"  style="box-shadow: 5px 5px 20px black;">
                            <div class="card-header text-right">
                                <img class="card-img" src="{{ asset('/img/square1.png') }}" alt="Card image" style="margin-left: 9em;">
                                <h2 class="d-none d-sm-block card-title text-white pr-3 pt-3">Register</h2>
                                <h2 class="d-block d-sm-none card-title text-white smaller" style="font-size: 4.5em;">Register</h2>
                            </div>
                            <div class="card-body">
                                <form action="#" class="form">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="tim-icons icon-badge"></i>
                                            </div>
                                        </div>
                                        <input class="form-control" type="text" name="name" id="name" placeholder="Full Name">
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="tim-icons icon-single-02"></i>
                                            </div>
                                        </div>
                                        <input class="form-control" type="text" name="username" id="username" placeholder="Username">
                                    </div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="tim-icons icon-lock-circle"></i>
                                            </div>
                                        </div>
                                        <input class="form-control" type="password" name="password" id="password" placeholder="Password">
                                    </div>
                                    <div class="card-footer justify-content-center">
                                        <button type="button" id="register" class="btn btn-info btn-round btn-lg">Register</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $("#register").click(function(){
                console.log($("#name").val());
                
                $.ajax({
                    method: 'POST',
                    url: 'register',
                    data: {
                        'name': $("#name").val(),
                        'username': $("#username").val(),
                        'password': $("#password").val(),
                        '_token': _token
                    }
                }).done(function(data) {
                    
                    if(data['err_code'] == 0){
                        window.location.href = data["msg"];
                    } else {
                        $.notify({
                            message: data["msg"]
                        }, {
                            type: 'danger',
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            },
                            z_index: 9999
                        });
                    }
                })
            })
        })
    </script>
@endsection