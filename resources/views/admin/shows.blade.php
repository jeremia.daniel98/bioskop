@extends('layouts.admin_main')

@section('title')
    Shows
@endsection 

@section('content')


    <!-- Main -->
    <div class="wrapper">
		<div class="section">
			<div class="container">
				<div class="row">
                    <div class="col" style="margin-top: 2em;">
						<h2>Shows</h2>
					</div>
					<div class="col" style="margin-top: 2em;">
						<span class="pull-right">
							<button type="button" class="btn btn-fab btn-round btn-success add_show" id="add_show">
								<!-- <i class="tim-icons icon-simple-add"></i> -->
								Add Shows
							</button>
						</span> 
					</div>
				</div>
				<div class="row m-2">
					<div class="col" style="overflow-y:auto">
						<table class="table shows" id="shows">
							<thead>
								<tr>
                                    <th class="text-center">Id</th>
                                    <th class="text-center">Cover</th>
                                    <th class="text-center">Movie Title</th>
                                    <th class="text-center">Cinema</th>
                                    <th class="text-center">Studio</th>
                                    <th class="text-center">City</th>
                                    <th class="text-center">Show Date</th>
                                    <th class="text-center">Show Time</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Seats</th>
                                    <th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody class="table_data">

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </div>

<!-- Modal Add -->
<div class="modal fade add_modal" id="add_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg-dark">
            <div class="modal-header">
                <h4 class="modal-title text-white" id="add_modal">Add</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- list of cities, dropdown -->
                <div class="form-group">
                    <label for="city-list">City List</label>
                        <select class="form-control bg-dark" id="city-list">
                            
                        </select>
                </div>
                <!-- list of cinema, dropdown -->
                <div class="form-group">
                    <label for="cinema-list">Cinema List</label>
                        <select class="form-control  bg-dark" id="cinema-list">
                            <option disabled selected value> -- select a cinema -- </option>
                        </select>
                </div>
                <!-- list of studio, dropdown -->
                <div class="form-group">
                    <label for="studio-list">Studio List</label>
                        <select class="form-control  bg-dark" id="studio-list">
                            <option disabled selected value> -- select a studio -- </option>
                        </select>
                </div>
                <!-- list of films, dropdown -->
                <div class="form-group">
                    <label for="film-list">Movie List</label>
                        <select class="form-control bg-dark" id="film-list">
                            <option disabled selected value> -- select a movie -- </option>
                        </select>
                </div>
                <!-- start date -->
                <div class="form-group">
                    <label for="start_date">Start Date</label>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" id="start_date" placeholder="dd">
                            </div>
                        <div class="col">
                            <input type="text" class="form-control" id="start_month" placeholder="mm">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" id="start_year" placeholder="yyyy">
                        </div>
                    </div>
                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                </div>
                <!-- end date -->
                <div class="form-group">
                    <label for="end_date">End Date</label>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" id="end_date" placeholder="dd">
                            </div>
                        <div class="col">
                            <input type="text" class="form-control" id="end_month" placeholder="mm">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" id="end_year" placeholder="yyyy">
                        </div>
                    </div>
                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                </div>
                <!-- start time -->
                <div class="form-group">
                    <label for="start_time">Start time</label>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" id="start_hour" placeholder="hh">
                            </div>
                        <div class="col">
                            <input type="text" class="form-control" id="start_minute" placeholder="mm">
                        </div>
                    </div>
                </div>
                <!-- end time -->
                <!-- price (weekdays, sat, sun) -->
                <div class="form-group">
                    <label for="prices">Price</label>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" id="weekday_price" placeholder="weekday">
                            </div>
                        <div class="col">
                            <input type="text" class="form-control" id="saturday_price" placeholder="saturday">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" id="sunday_price" placeholder="sunday">
                        </div>
                    </div>
                    <small id="price help" class="form-text text-muted">Weekend prices are optional</small>
                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success add_button">Add</button>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Add -->

<!-- Modal Edit -->
<div class="modal fade edit_modal" id="edit_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg-dark">
            <div class="modal-header">
                <h4 class="modal-title text-white" id="add_modal">Edit</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- id -->
                <div class="form-group">
                    <input type="hidden" class="form-control" id="edit_id">
                </div>

                <!-- list of cities, dropdown -->
                <div class="form-group">
                    <label for="edit-city-list">City List</label>
                        <select class="form-control bg-dark" id="edit-city-list">
                            
                        </select>
                </div>
                <!-- list of cinema, dropdown -->
                <div class="form-group">
                    <label for="edit-cinema-list">Cinema List</label>
                        <select class="form-control  bg-dark" id="edit-cinema-list">
                        {{-- <option disabled selected value> -- select a cinema -- </option> --}}
                        </select>
                </div>
                <!-- list of studio, dropdown -->
                <div class="form-group">
                    <label for="edit-studio-list">Studio List</label>
                        <select class="form-control  bg-dark" id="edit-studio-list">
                            <option disabled selected value> -- select a studio -- </option>
                        </select>
                </div>
                <!-- list of films, dropdown -->
                <div class="form-group">
                    <label for="edit-film-list">Movie List</label>
                        <select class="form-control bg-dark" id="edit-film-list">
                            <option disabled selected value> -- select a movie -- </option>
                        </select>
                </div>
                <!-- start date -->
                <div class="form-group">
                    <label for="edit-date">Date</label>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" id="edit-date" placeholder="dd">
                            </div>
                        <div class="col">
                            <input type="text" class="form-control" id="edit-month" placeholder="mm">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" id="edit-year" placeholder="yyyy">
                        </div>
                    </div>
                    <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                </div>
                <!-- start time -->
                <div class="form-group">
                    <label for="start_time">Start time</label>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" id="edit-start_hour" placeholder="hh">
                            </div>
                        <div class="col">
                            <input type="text" class="form-control" id="edit-start_minute" placeholder="mm">
                        </div>
                    </div>
                </div>
                <!-- end time -->
                <!-- price (weekdays, sat, sun) -->
                <div class="form-group">
                    <label for="prices">Price</label>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" id="edit-price" placeholder="weekday">
                        </div>
                    </div>
                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success edit_button">Edit</button>
            </div>
        </div>
    </div>
</div>

<!-- End Modal Edit -->
  



@endsection 

@section('script')
<script>
function tableInit(){
    $('#shows').DataTable({
        ajax: '/bioskop/public/api/admin/shows',
        columns: [
            {data: 'id'},
            {data: 'film.filename'},
            {data: 'film.title'},
            {data: 'studio.cinema.name'},
            {data: 'studio.name'},
            {data: 'studio.cinema.city.name'},
            {data: 'show_date'},
            {data: 'start_time'},
            {data: 'price'},
            {
                render: function(data, type, row, meta){
                    return `
                        <a href="/bioskop/public/admin/seats/`+row['id']+`">Show</a>
                    `
                }
            },
            {
                data: null,
                defaultContent: `
                    <button class="btn btn-success btn-fab btn-icon btn-round btn-sm edit" data-toggle="modal" data-target="#edit_modal" type="button">
                        <i class="tim-icons icon-pencil"></i>  
                    </button>
                    <button class="btn btn-success btn-fab btn-icon btn-round btn-sm delete" type="button">
                        <i class="tim-icons icon-simple-remove"></i>
                    </button>
                `
            }
        ]
    });
}
function reloadData(){
    $("#shows").DataTable().ajax.reload()
}
function getCities(){
    $.ajax({
        method: "GET",
        url: "/bioskop/public/api/city",
        data: {
            _token: _token
        }
    }).done(function(data){
        // console.log(data);
        $("#city-list").html('')
        $("#city-list").append("<option disabled selected value> -- select a city -- </option>");
        data.forEach(function(value, index) {
            var row = '<option value="'+value['id']+'">'+ value['name'] +"</option>"
            $("#city-list").append(row);
        })
        // $("#city-list").selectpicker('refresh');
        //$("option:selected").removeAttr("selected");
    })
}
function getCinemasByCity($city_id){
    $.ajax({
        method: "POST",
        url: "/bioskop/public/api/admin/cinema",
        data: {
            _token: _token,
            id: $city_id
        }
    }).done(function(data){
        // console.log(data);
        $("#cinema-list").html('');
        $("#cinema-list").append("<option disabled selected value> -- select a cinema -- </option>");
        data.forEach(function(value, index) {
            var row = '<option value="'+value['id']+'">' + value['name'] +"</option>"
            $("#cinema-list").append(row);
        })
    })
}
function getStudiosByCinema($cinema_id){
    $.ajax({
        method: "POST",
        url: "/bioskop/public/api/admin/studio",
        data: {
            _token: _token,
            id: $cinema_id
        }
    }).done(function(data){
        // console.log(data);
        $("#studio-list").html('');
        $("#studio-list").append("<option disabled selected value> -- select a studio -- </option>");
        data.forEach(function(value, index) {
            var row = '<option value="'+value['id']+'">' + value['name'] +"</option>"
            $("#studio-list").append(row);
        })
    })
}
function getMovies(){
    $.ajax({
        method: "GET",
        url: "/bioskop/public/api/admin/film",
        data: {
            _token: _token
        }
    }).done(function(data){
        // console.log("movi");
        // console.log(data);
        $("#film-list").html('');
        $("#film-list").append("<option disabled selected value> -- select a movie -- </option>");
        data.forEach(function(value, index) {
            // alert(value['title']);
            var row = '<option value="'+value['id']+'">' + value['title'] +"</option>"
            $("#film-list").append(row);
        })
    })
}
function getCitiesEdit(){
    $.ajax({
        method: "GET",
        url: "/bioskop/public/api/city",
        data: {
            _token: _token
        }
    }).done(function(data){
        // console.log(data);
        $("#edit-city-list").html('')
        $("#edit-city-list").append("<option disabled value> -- select a city -- </option>");
        data.forEach(function(value, index) {
            var row = '<option value="'+value['id']+'" data-name="'+value['name'].trim()+'">'+ value['name'] +"</option>"
            $("#edit-city-list").append(row);
        })
        // $("#city-list").selectpicker('refresh');
        //$("option:selected").removeAttr("selected");
    })
}
function getCinemasByCityEdit($city_id){
    $.ajax({
        method: "POST",
        url: "/bioskop/public/api/admin/cinema",
        data: {
            _token: _token,
            id: $city_id
        }
    }).done(function(data){
        // console.log(data);
        $("#edit-cinema-list").html('');
        $("#edit-cinema-list").append("<option disabled value> -- select a cinema -- </option>");
        //alert(":)");
        data.forEach(function(value, index) {
            //alert(value['id']);
            var row = '<option value="'+value['id']+'" data-name="'+value['name'].trim()+'">'+ value['name'] +"</option>";
            $("#edit-cinema-list").append(row);
        })
    })
    //alert($('#edit-cinema-list option[data-name="' + $c + '"]').val());
    $("#edit-cinema-list").val($c);
    //$('#edit-cinema-list option[data-name="' + $c + '"]').prop('selected', true);
    
    getStudiosByCinemaEdit($("#edit-cinema-list option:selected").val(), $cc);
}
function getStudiosByCinemaEdit($cinema_id){
    $.ajax({
        method: "POST",
        url: "/bioskop/public/api/admin/studio",
        data: {
            _token: _token,
            id: $cinema_id
        }
    }).done(function(data){
        // console.log(data);
        $("#edit-studio-list").html('');
        $("#edit-studio-list").append("<option disabled value> -- select a studio -- </option>");
        // alert("edit studio");
        data.forEach(function(value, index) {
            var row = '<option value="'+value['id']+'" data-name="'+value['name'].trim()+'">' + value['name'] +"</option>"
            $("#edit-studio-list").append(row);
        })
    })
}
function getMoviesEdit(){
    $.ajax({
        method: "GET",
        url: "/bioskop/public/api/admin/film",
        data: {
            _token: _token
        }
    }).done(function(data){
        // console.log("movi");
        // console.log(data);
        $("#edit-film-list").html('');
        $("#edit-film-list").append("<option disabled value> -- select a movie -- </option>");
        data.forEach(function(value, index) {
            // alert(value['title']);
            var row = '<option value="'+value['id']+'" data-name="'+value['title'].trim()+'">' + value['title'] +"</option>"
            $("#edit-film-list").append(row);
        })
    })
}
function empty_modal(){
    
}
function reset_dropdown(){

}
$(document).ready(function(){
    tableInit();
    getCities();
    getMovies();
    getCitiesEdit();
    getMoviesEdit();
    
    $(".shows").on('click', '.edit', function() {
        val = $("#shows").DataTable().row( $(this).parents('tr') ).data()
        console.log(val);
        $("#edit_id").val(val.id);
        $('#edit-city-list option[data-name="' + val.studio.cinema.city.name + '"]').prop('selected', true);
        // $("#edit-city-list select").data(val.studio.cinema.city.name);
        // alert($("#edit-city-list option:selected").val());
        getCinemasByCityEdit($("#edit-city-list option:selected").val());
        // alert($('#edit-cinema-list option[data-name="' + val.studio.cinema.name + '"]').html());
        // alert(val.studio.cinema.name);
        $('#edit-cinema-list').val("'"+val.studio.cinema.id+"'");
        $('#edit-cinema-list option[data-name="' + val.studio.cinema.name + '"]').selected = true;
        //$('#edit-cinema-list option[data-name="' + $c + '"]').prop('selected', true);
        
        getStudiosByCinemaEdit($("#edit-cinema-list option:selected").val());
        // alert($("#edit-cinema-list option:selected").val());
        // alert(val.studio.name);
        $('#edit-studio-list option[data-name="' + val.studio.name + '"]').prop('selected', true);
        
        $('#edit-film-list option[data-name="' + val.film.title + '"]').prop('selected', true);
       
        // alert(val.show_date);
        a = val.show_date.split("-");
        $("#edit-date").val(a[2]);
        $("#edit-month").val(a[1]);
        $("#edit-year").val(a[0]);

        a = val.start_time.split(":");
        $("#edit-start_hour").val(a[0]);
        $("#edit-start_minute").val(a[1]);
        $("#edit-price").val(val.price);
        $("#edit_modal").modal("show");

    });
    $(".shows").on('click', '.delete', function(){
        val = $("#shows").DataTable().row( $(this).parents('tr') ).data()
        console.log(val);
        $.ajax({
            method: "POST",
            url: "/bioskop/public/api/admin/delete_show",
            data:{
                _token: _token,
                id : val.id
            }
        }).done(function(data){
            if (data['err_code'] != 0){
                $.notify({
                    message: data["msg"]
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    z_index: 9999
                });
            } else {
                $.notify({
                    message: data["msg"]
                    }, {
                        type: 'success',
                        animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    z_index: 9999
                });
            }
            reloadData();
        });
    });
    $("#add_show").click(function() {
        $(".add_modal").modal("show");
    });
    $("#city-list").change(function(){
        getCinemasByCity($(this).val());
    });
    $("#cinema-list").change(function(){
        getStudiosByCinema($(this).val());
    });
    $(".add_button").click(function(){
        // alert($("#studio-list option:selected").val());
        // alert($("#film-list option:selected").val());
        // alert($("#start_date").val() + "/" + $("#start_month").val() + "/" + $("#start_year").val());
        // alert($("#end_date").val() + "/" + $("#end_month").val() + "/" + $("#end_year").val());
        // alert($("#weekday_price").val());
        $.ajax({
            method: "POST",
            url: "/bioskop/public/api/admin/add_show",
            data:{
                _token: _token,
                // city_id : $("#city-list option:selected").val(),
                // cinema_id : $("#cinema-list option:selected").val(),
                studio_id : $("#studio-list option:selected").val(),
                film_id : $("#film-list option:selected").val(),
                start_date : $("#start_year").val() + "-" + $("#start_month").val() + "-" + $("#start_date").val(),
                end_date : $("#end_year").val() + "-" + $("#end_month").val() + "-" + $("#end_date").val(),
                start_time : $("#start_hour").val() + ":" + $("#start_minute").val(),
                weekday_price : $("#weekday_price").val(),
                sat_price : $("#saturday_price").val(),
                sun_price : $("#sunday_price").val()
            }
        }).done(function(data){
            console.log(data);
            if (data['err_code'] != 0){
                if (data['msg']=='') {
                        var message ='Shows at date : ';
                    data['col'].forEach(function(value, index){
                        message = message + value['show_date'] + " ";
                    });
                    message = message + "can't be added!";
                } else {
                    message = data['msg'];
                }
                $.notify({
                    message: message
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    z_index: 9999
                });
            } else {
                $.notify({
                    message: data["msg"]
                    }, {
                        type: 'success',
                        animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    z_index: 9999
                });
                $("#add_modal").modal("hide");
            }
            reloadData() 
            empty_modal();
        });
    });

    $("#edit-city-list").change(function(){
        getCinemasByCityEdit($(this).val());
    });
    $("#edit-cinema-list").change(function(){
        getStudiosByCinemaEdit($(this).val());
    });
    $(".edit_button").click(function(){
        $.ajax({
            method: "POST",
            url: "/bioskop/public/api/admin/edit_show",
            data:{
                _token: _token,
                id : $("#edit_id").val(),
                studio_id : $("#edit-studio-list option:selected").val(),
                film_id : $("#edit-film-list option:selected").val(),
                date : $("#edit-year").val() + "-" + $("#edit-month").val() + "-" + $("#edit-date").val(),
                start_time : $("#edit-start_hour").val() + ":" + $("#edit-start_minute").val(),
                price : $("#edit-price").val()
            }
        }).done(function(data){
            console.log(data);
            if (data['err_code'] != 0){
                $.notify({
                    message: 'cant edit'
                }, {
                    type: 'danger',
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    z_index: 9999
                });
            } else {
                $.notify({
                    message: data["msg"]
                    }, {
                        type: 'success',
                        animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    z_index: 9999
                });
                $("#edit_modal").modal("hide");
            }
            reloadData();
        });
    });
    
});
</script>
@endsection

@section('show')
    active
@endsection