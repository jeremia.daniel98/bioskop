@extends('layouts.admin_main')

@section('title')
    Studios
@endsection 

@section('content')
    <!-- Main -->
    <div class="wrapper">
		<div class="section">
			<div class="container">
				<div class="row">
                    <div class="col" style="margin-top: 2em;">
						<h2>Studios</h2>
					</div>
					<div class="col" style="margin-top: 2em;">
						<span class="pull-right">
							<button type="button" class="btn btn-fab btn-round btn-success add_studio" id="add_studio" data-toggle="modal" data-target="#add_modal">
								<!-- <i class="tim-icons icon-simple-add"></i> -->
								Add Studio
							</button>
						</span> 
					</div>
				</div>
				<div class="row m-2">
					<div class="col" style="overflow-y:auto">
						<table class="table studios" id="studios">
							<thead>
								<tr>
                                    <th class="text-center">Id</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Row Size</th>
                                    <th class="text-center">Col Size</th>
                                    <th class="text-center">Cinema</th>
                                    <th class="text-center">City</th>
                                    <th class="text-center" style="width: 5%">Action</th>
								</tr>
							</thead>
							<tbody class="table_data">

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </div>
    {{-- End Main --}}



    <!-- Modal Add -->
    <div class="modal fade add_modal" id="add_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-dark">
                <div class="modal-header">
                    <h4 class="modal-title text-white" id="add_modal">Add</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/bioskop/public/api/studio" id="form_add" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control" type="text" name="name" id="name">
                        </div>
                        <div class="form-group">
                            <label for="name">Row Size</label>
                            <input class="form-control" type="text" name="row" id="row">
                        </div>
                        <div class="form-group">
                            <label for="name">Col Size</label>
                            <input class="form-control" type="text" name="col" id="col">
                        </div>
                        <div class="form-group">
                            <label for="name">City</label>
                            <select class="form-control" name="city" id="city">
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Cinema</label>
                            <select class="form-control" name="cinema" id="cinema">
                                
                            </select>
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success add_button text-right">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Add -->


    <!-- Modal Edit -->
    <div class="modal fade edit_modal" id="edit_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-dark">
                <div class="modal-header">
                    <h4 class="modal-title text-white" id="edit_modal">Edit</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/bioskop/public/api/studio" id="form_edit" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control" type="text" name="name" id="ename">
                        </div>
                        <div class="form-group">
                            <label for="name">Row Size</label>
                            <input class="form-control" type="text" name="row" id="erow">
                        </div>
                        <div class="form-group">
                            <label for="name">Col Size</label>
                            <input class="form-control" type="text" name="col" id="ecol">
                        </div>
                        <div class="form-group">
                            <label for="name">City</label>
                            <select class="form-control" name="city" id="ecity">
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Cinema</label>
                            <select class="form-control" name="cinema" id="ecinema">
                                
                            </select>
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success edit_button text-right">Edit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal edit -->


@endsection 

@section('script')
    <script>
        function reloadData(){
            $("#studios").DataTable().ajax.reload()
        }
        $(document).ready(function() {
            $("#studios").DataTable({
                ajax: '/bioskop/public/api/studio',
                columns: [
                    {data: 'id'},
                    {data: 'name'},
                    {data: 'row_size'},
                    {data: 'col_size'},
                    {data: 'cinema.name'},
                    {data: 'cinema.city.name'},
                    {
                        render: function(data, type, row, meta){
                            return `
                            <button class="btn btn-success btn-fab btn-icon btn-round btn-sm edit" data-id="`+ row.id +`" data-toggle="modal" data-target="#edit_modal" type="button">
                                <i class="tim-icons icon-pencil"></i>  
                            </button>
                            <button class="btn btn-success btn-fab btn-icon btn-round btn-sm delete" data-id="`+ row.id +`" type="button">
                                <i class="tim-icons icon-simple-remove"></i>
                            </button>
                            `
                        }
                    }
                ]
            });

            $("#studios").on('click', '.edit', function() {
                $("#form_edit").data('id', $(this).data('id'))
                $("#ecity").html('')
                $.ajax({
                    url: '/bioskop/public/api/city',
                    method: 'GET',
                    async: false,
                    success: function(data){
                        data.forEach(function(value, index){
                            var row = `
                                <option value="`+value['id']+`">`+value['name']+`</option>
                            `
                            $("#ecity").append(row)
                        })
                    }
                })
                $("#ecinema").html('')
                $.ajax({
                    url: '/bioskop/public/api/admin/cinema',
                    method: 'POST',
                    async: false,
                    data:{
                        id: $("#ecity").val()
                    },
                    success: function(data){
                        data.forEach(function(value, index){
                            var row = `
                                <option value="`+value['id']+`">`+value['name']+`</option>
                            `
                            $("#ecinema").append(row)
                        })
                    }
                })

                $.ajax({
                    url: '/bioskop/public/api/studiobyid',
                    method: 'GET',
                    data: {
                        id: $(this).data('id')
                    },
                    success: function(data){
                        console.log(data)
                        $("#ename").val(data['name'])
                        $("#erow").val(data['row_size'])
                        $("#ecol").val(data['col_size'])
                        $("#ecity").val(data['cinema']['city']['id'])
                        $("#ecinema").html('')
                        $.ajax({
                            url: '/bioskop/public/api/admin/cinema',
                            method: 'POST',
                            async: false,
                            data:{
                                id: $("#ecity").val()
                            },
                            success: function(data){
                                data.forEach(function(value, index){
                                    var row = `
                                        <option value="`+value['id']+`">`+value['name']+`</option>
                                    `
                                    $("#ecinema").append(row)
                                })
                            }
                        })
                        $("#ecinema").val(data['cinema']['id'])
                    },
                    error: function(){
                        console.log('FAIL')
                    }
                })

            })

            $("#form_edit").on('submit', function(event) {
                event.preventDefault()
                $.ajax({
                    url: '/bioskop/public/api/studio/' + $(this).data('id'),
                    method: 'POST',
                    data: new FormData(this),
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data){
                        console.log(data)
                        if(data['err_code'] == 0){
                            but = 'success'
                        } else{
                            but = 'danger'
                        }
                        $.notify({
                            message: data['msg']
                        }, {
                            type: but,
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            },
                            z_index: 9999
                        });
                        reloadData()
                        $("#edit_modal").modal('hide')
                    },
                    error: function() {
                        $.notify({
                            message: "Server Error"
                        }, {
                            type: 'danger',
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            },
                            z_index: 9999
                        });
                    }
                })
            })

            $("#ecity").change(function() {
                $("#ecinema").html('')
                $.ajax({
                    url: '/bioskop/public/api/admin/cinema',
                    method: 'POST',
                    async: false,
                    data:{
                        id: $("#ecity").val()
                    },
                    success: function(data){
                        data.forEach(function(value, index){
                            var row = `
                                <option value="`+value['id']+`">`+value['name']+`</option>
                            `
                            $("#ecinema").append(row)
                        })
                    }
                })
            })


            $("#add_studio").click(function() {
                $("#city").html('')
                $.ajax({
                    url: '/bioskop/public/api/city',
                    method: 'GET',
                    async: false,
                    success: function(data){
                        data.forEach(function(value, index){
                            var row = `
                                <option value="`+value['id']+`">`+value['name']+`</option>
                            `
                            $("#city").append(row)
                        })
                    }
                })
                $("#cinema").html('')
                $.ajax({
                    url: '/bioskop/public/api/admin/cinema',
                    method: 'POST',
                    async: false,
                    data:{
                        id: $("#city").val()
                    },
                    success: function(data){
                        data.forEach(function(value, index){
                            var row = `
                                <option value="`+value['id']+`">`+value['name']+`</option>
                            `
                            $("#cinema").append(row)
                        })
                    }
                })
            })
            $("#city").change(function() {
                $("#cinema").html('')
                $.ajax({
                    url: '/bioskop/public/api/admin/cinema',
                    method: 'POST',
                    data:{
                        id: $("#city").val()
                    },
                    success: function(data){
                        data.forEach(function(value, index){
                            var row = `
                                <option value="`+value['id']+`">`+value['name']+`</option>
                            `
                            $("#cinema").append(row)
                        })
                    }
                })
            })

            $("#studios").on('click', '.delete', function() {
                var r = confirm("Are you sure you want to delete this? This will delete ALL related entry!")
                if(r){
                        $.ajax({
                        url: '/bioskop/public/api/studio/' + $(this).data('id'),
                        method: "DELETE",
                        success: function(data){
                            $.notify({
                                message: data['msg']
                            }, {
                                type: 'success',
                                animate: {
                                    enter: 'animated fadeInDown',
                                    exit: 'animated fadeOutUp'
                                },
                                z_index: 9999
                            });
                            reloadData()
                        }
                    })
                }
            })

            $("#form_add").on('submit', function(event) {
                event.preventDefault()
                $.ajax({
                    url: '/bioskop/public/api/studio',
                    method: 'POST',
                    data: new FormData(this),
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data){
                        if(data['err_code'] == 0){
                            but = 'success'
                        } else{
                            but = 'danger'
                        }
                        $.notify({
                            message: data['msg']
                        }, {
                            type: but,
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            },
                            z_index: 9999
                        });
                        reloadData()
                        $("#add_modal").modal('hide')
                    },
                    error: function(){
                        $.notify({
                            message: "Server Error"
                        }, {
                            type: 'danger',
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            },
                            z_index: 9999
                        });
                        
                    }
                })
            })
        })
    </script>
@endsection

@section('studio')
    active
@endsection