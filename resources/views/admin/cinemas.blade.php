@extends('layouts.admin_main')

@section('title')
    Cinemas
@endsection 

@section('content')
    <!-- Main -->
    <div class="wrapper">
		<div class="section">
			<div class="container">
				<div class="row">
                    <div class="col" style="margin-top: 2em;">
						<h2>Cinemas</h2>
					</div>
					<div class="col" style="margin-top: 2em;">
						<span class="pull-right">
							<button type="button" class="btn btn-fab btn-round btn-success add_cinema" id="add_cinema" data-toggle="modal" data-target="#add_modal">
								<!-- <i class="tim-icons icon-simple-add"></i> -->
								Add Cinema
							</button>
						</span> 
					</div>
				</div>
				<div class="row m-2">
					<div class="col" style="overflow-y:auto">
						<table class="table cinemas" id="cinemas">
							<thead>
								<tr>
                                    <th class="text-center">Id</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Address</th>
                                    <th class="text-center">City</th>
                                    <th class="text-center" style="width: 5%">Action</th>
								</tr>
							</thead>
							<tbody class="table_data">

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </div>
    {{-- End Main --}}

    <!-- Modal Add -->
    <div class="modal fade add_modal" id="add_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-dark">
                <div class="modal-header">
                    <h4 class="modal-title text-white" id="add_modal">Add</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/bioskop/public/api/cinema" id="form_add" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control" type="text" name="name" id="name">
                        </div>
                        <div class="form-group">
                            <label for="name">Address</label>
                            <input class="form-control" type="text" name="address" id="address">
                        </div>
                        <div class="form-group">
                            <label for="name">City</label>
                            <select class="form-control" name="city" id="city">
                                
                            </select>
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success add_button text-right">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Add -->

    <!-- Modal edit -->
    <div class="modal fade edit_modal" id="edit_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-dark">
                <div class="modal-header">
                    <h4 class="modal-title text-white" id="edit_modal">Edit</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/bioskop/public/api/cinema" id="form_edit" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control" type="text" name="name" id="ename">
                        </div>
                        <div class="form-group">
                            <label for="name">Address</label>
                            <input class="form-control" type="text" name="address" id="eaddress">
                        </div>
                        <div class="form-group">
                            <label for="name">City</label>
                            <select class="form-control" name="city" id="ecity">
                                
                            </select>
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success edit_button text-right">Edit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal edit -->


@endsection 

@section('script')
    <script>
        function reloadData(){
            $("#cinemas").DataTable().ajax.reload()
        }
        $(document).ready(function() {
            $("#cinemas").DataTable({
                ajax: '/bioskop/public/api/cinema',
                columns: [
                    {data: 'id'},
                    {data: 'name'},
                    {data: 'address'},
                    {data: 'city.name'},
                    {
                        render: function(data, type, row, meta){
                            return `
                            <button class="btn btn-success btn-fab btn-icon btn-round btn-sm edit" data-id="`+ row.id +`" data-toggle="modal" data-target="#edit_modal" type="button">
                                <i class="tim-icons icon-pencil"></i>  
                            </button>
                            <button class="btn btn-success btn-fab btn-icon btn-round btn-sm delete" data-id="`+ row.id +`" type="button">
                                <i class="tim-icons icon-simple-remove"></i>
                            </button>
                            `
                        }
                    }
                ]
            });


            $("#add_cinema").click(function() {
                $("#city").html('')
                $.ajax({
                    url: '/bioskop/public/api/city',
                    method: 'GET',
                    success: function(data){
                        data.forEach(function(value, index){
                            var row = `
                                <option value="`+value['id']+`">`+value['name']+`</option>
                            `
                            $("#city").append(row)
                        })
                    }
                })
            })

            $("#cinemas").on('click', '.edit', function() {
                $("#ecity").html('')
                $.ajax({
                    url: '/bioskop/public/api/city',
                    method: 'GET',
                    async: false,
                    success: function(data){
                        data.forEach(function(value, index){
                            var row = `
                                <option value="`+value['id']+`">`+value['name']+`</option>
                            `
                            $("#ecity").append(row)
                        })
                    }
                })

                $.ajax({
                    url: '/bioskop/public/api/cinemaid',
                    method: 'GET',
                    async: false,
                    data: {
                        id: $(this).data('id')
                    },
                    success: function(data){
                        $("#ename").val(data['name'])
                        $("#eaddress").val(data['address'])
                        $("#ecity").val(data['city_id'])
                    }
                })
                $("#form_edit").data('id', $(this).data('id'))
            })


            $("#form_add").on('submit', function(event) {
                event.preventDefault()
                $.ajax({
                    url: '/bioskop/public/api/cinema',
                    method: 'POST',
                    data: new FormData(this),
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data){
                        if(data['err_code'] == 0){
                            but = 'success'
                        } else{
                            but = 'danger'
                        }
                        $.notify({
                            message: data['msg']
                        }, {
                            type: but,
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            },
                            z_index: 9999
                        });
                        reloadData()
                        $("#add_modal").modal('hide')
                    },
                    error: function(){
                        $.notify({
                            message: "Server Error"
                        }, {
                            type: 'danger',
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            },
                            z_index: 9999
                        });
                        
                    }
                })
            })

            $("#form_edit").on('submit', function(event) {
                event.preventDefault()
                $.ajax({
                    url: '/bioskop/public/api/cinema/' + $(this).data('id'),
                    method: 'POST',
                    data: new FormData(this),
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data){
                        console.log(data)
                        if(data['err_code'] == 0){
                            but = 'success'
                        } else{
                            but = 'danger'
                        }
                        $.notify({
                            message: data['msg']
                        }, {
                            type: but,
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            },
                            z_index: 9999
                        });
                        reloadData()
                        $("#edit_modal").modal('hide')
                    },
                    error: function() {
                        $.notify({
                            message: "Server Error"
                        }, {
                            type: 'danger',
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            },
                            z_index: 9999
                        });
                    }
                })
            })


            $("#cinemas").on('click', '.delete', function() {
                var r = confirm("Are you sure you want to delete this? This will delete ALL related entry!")
                if(r){
                        $.ajax({
                        url: '/bioskop/public/api/cinema/' + $(this).data('id'),
                        method: "DELETE",
                        success: function(data){
                            $.notify({
                                message: data['msg']
                            }, {
                                type: 'success',
                                animate: {
                                    enter: 'animated fadeInDown',
                                    exit: 'animated fadeOutUp'
                                },
                                z_index: 9999
                            });
                            reloadData()
                        }
                    })
                }
            })


        })
    </script>
@endsection

@section('cinema')
    active
@endsection