@extends('layouts.admin_main')

@section('title')
    Vouchers
@endsection 

@section('content')
    <!-- Main -->
    <div class="wrapper">
		<div class="section">
			<div class="container">
				<div class="row">
                    <div class="col" style="margin-top: 2em;">
						<h2>Vouchers</h2>
					</div>
					<div class="col" style="margin-top: 2em;">
						<span class="pull-right">
							<button type="button" class="btn btn-fab btn-round btn-success add_voucher" id="add_voucher" data-toggle="modal" data-target="#add_modal">
								<!-- <i class="tim-icons icon-simple-add"></i> -->
								Add Vouchers
							</button>
						</span> 
					</div>
				</div>
				<div class="row m-2">
					<div class="col" style="overflow-y:auto">
						<table class="table vouchers" id="vouchers">
							<thead>
								<tr>
                                    <th class="text-center">Id</th>
                                    <th class="text-center">Code</th>
                                    <th class="text-center">Value</th>
                                    <th class="text-center">User ID</th>
								</tr>
							</thead>
							<tbody class="table_data">

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </div>
    {{-- End Main --}}

    <!-- Modal Add -->
    <div class="modal fade add_modal" id="add_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-dark">
                <div class="modal-header">
                    <h4 class="modal-title text-white" id="add_modal">Add</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/bioskop/public/api/studio" id="form_add" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Value</label>
                            <input class="form-control" type="text" name="value" id="value">
                        </div>

                        <div class="form-group">
                            <label for="name">Count</label>
                            <input class="form-control" type="text" name="count" id="count">
                        </div>

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success add_button text-right">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Add -->
@endsection 

@section('script')
    <script>
        function reloadData(){
            $("#vouchers").DataTable().ajax.reload()
        }
        $(document).ready(function() {
            $("#vouchers").DataTable({
                ajax: '/bioskop/public/api/voucher',
                columns: [
                    {data: 'id'},
                    {data: 'code'},
                    {data: 'value'},
                    {data: 'user_id'}
                ]
            });

            //Add
        $("#form_add").on('submit', function(event) {
                event.preventDefault()
                $.ajax({
                    url: '/bioskop/public/api/voucher',
                    method: 'POST',
                    data: new FormData(this),
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data){
                        if(data['err_code'] == 0){
                            but = 'success'
                        } else{
                            but = 'danger'
                        }
                        $.notify({
                            message: data['msg']
                        }, {
                            type: but,
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            },
                            z_index: 9999
                        });
                        reloadData()
                        $("#add_modal").modal('hide')
                    },
                    error: function(){
                        $.notify({
                            message: "Server Error"
                        }, {
                            type: 'danger',
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            },
                            z_index: 9999
                        });
                        
                    }
                })
            })
        //End Add


        })
    </script>
@endsection

@section('voucher')
    active
@endsection