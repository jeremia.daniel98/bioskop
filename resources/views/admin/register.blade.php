@extends('layouts.admin_main')

@section('title')
    Register
@endsection

@section('content')

    <!-- <div class="container">
        <div class="row">
            <div class="col-lg-5 offset-lg-7 offset-md-3" style="margin-top: 10em;">
                <div class="card card-register" >
                    <div class="card-header text-right">
                        <img class="card-img" src="{{ asset('/img/square-green-1.png') }}" alt="Card image" style="margin-left: 9em;">
                        <h2 class="card-title text-white pr-3 pt-3">Register</h2>
                    </div>
                    <div class="card-body">
                        <form action="#" class="form">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="tim-icons icon-badge"></i>
                                    </div>
                                </div>
                                <input class="form-control" type="text" name="name" id="name" placeholder="Full Name">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="tim-icons icon-single-02"></i>
                                    </div>
                                </div>
                                <input class="form-control" type="text" name="username" id="username" placeholder="Username">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="tim-icons icon-lock-circle"></i>
                                    </div>
                                </div>
                                <input class="form-control" type="password" name="password" id="password" placeholder="Password">
                            </div>
                            <div class="card-footer">
                                <button type="button" id="register" class="btn btn-success btn-round btn-lg">Register</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    

    <!-- Main -->
    <div class="wrapper">
		<div class="section">
			<div class="container">
				<div class="row">
                    <div class="col" style="margin-top: 2em;">
						<h2>Admins</h2>
					</div>
					<div class="col" style="margin-top: 2em;">
						<span class="pull-right">
							<button type="button" class="btn btn-fab btn-round btn-success add_admin" id="add_admin">
								<!-- <i class="tim-icons icon-simple-add"></i> -->
								Add Admin
							</button>
						</span> 
					</div>
				</div>
				<div class="row m-2">
					<div class="col" style="overflow-y:auto">
						<table class="table admins" id="admins">
							<thead>
								<tr>
									<th class="text-center" style="width: 8.33%">Id</th>
									<th class="text-center">Name</th>
									<th class="text-center">Username</th>
									<th class="text-center" style="width: 20%">Action</th>
								</tr>
							</thead>
							<tbody class="table_data">

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </div>
    
    <!-- Add Modal -->
    <div class="modal fade add_modal" id="add_modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content bg-dark">
				<div class="modal-header">
					<h5 class="modal-title text-white" id="add_modal">Add New Admin</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
                    <div class="form-group">
                        <label for="add_name">Name</label>
                        <input type="text" class="form-control" id="add_name" placeholder="Arthur Pendragon">
                    </div>
                    <div class="form-group">
                        <label for="add_username">Username</label>
                        <input type="text" class="form-control" id="add_username" placeholder="Arthur211">
                    </div>
                    <div class="form-group">
                        <label for="add_password">Password</label>
                        <input type="password" class="form-control" id="add_password">
                    </div>
                    <div class="form-group">
                        <label for="add_cpassword">Confirm Password</label>
                        <input type="password" class="form-control" id="add_cpassword">
                    </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary add_button">Add</button>
				</div>
			</div>
		</div>
	</div>
    <!-- End Add Modal -->

    <!-- Edit Modal -->
    <div class="modal fade edit_modal" id="edit_modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content bg-dark">
				<div class="modal-header">
					<h5 class="modal-title text-white" id="edit_modal">Edit Admin</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" class="form-control" id="edit_id">
                    </div>
                    <div class="form-group">
                        <label for="edit_name">Name</label>
                        <input type="text" class="form-control" id="edit_name" placeholder="Arthur Pendragon">
                    </div>
                    <div class="form-group">
                        <label for="edit_username">Username</label>
                        <input type="text" class="form-control" id="edit_username" placeholder="Arthur211" disabled>
                    </div>
                    <div class="form-group">
                        <label for="edit_old_password">Old Password</label>
                        <input type="password" class="form-control" id="edit_old_password">
                    </div>
                    <div class="form-group">
                        <label for="edit_password">New Password</label>
                        <input type="password" class="form-control" id="edit_password">
                    </div>
                    <div class="form-group">
                        <label for="edit_cpassword">Confirm New Password</label>
                        <input type="password" class="form-control" id="edit_cpassword">
                    </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary edit_button" id="edit_button">Edit</button>
				</div>
			</div>
		</div>
	</div>
    <!-- End Edit Modal -->
@endsection

@section('script')
    <!-- <script>
        $(document).ready(function() {
            $("#register").click(function(){
                console.log($("#name").val());
                
                $.ajax({
                    method: 'POST',
                    url: 'admin/admin',
                    data: {
                        'name': $("#name").val(),
                        'username': $("#username").val(),
                        'password': $("#password").val(),
                        '_token': _token
                    }
                }).done(function(data) {
                    
                    if(data['err_code'] == 0){
                        window.location.href = '/bioskop/public'+data["msg"];
                    } else {
                        $.notify(data["msg"]);
                    }
                })
            })
        })
    </script> -->
<script>
function get_data(){
    $.ajax({
        method: "GET",
        url: "/bioskop/public/api/admin/admin",
        data:{
            _token: _token
        }
    }).done(function(data){
        console.log("ok");
        $(".table_data").html(""); //mbo gaisa
        if (data.length == 0){
            console.log(data);
            $(".admins").append(`
                <tr>
                    <td class="colspan-8">No Admin Yet</td>
                </tr>
            `)
        } else {
            table = $("#admins").DataTable({
                data: data,
                columns: [
                    {data: 'id'},
                    {data: 'name'},
                    {data: 'username'},
                    {
                        data: null,
                        defaultContent: `
                            <button class="btn btn-success btn-fab btn-icon btn-round btn-sm edit" data-toggle="modal" data-target="#edit_modal" type="button">
                                <i class="tim-icons icon-pencil"></i>  
                            </button>
                            <button class="btn btn-success btn-fab btn-icon btn-round btn-sm delete" type="button">
                                <i class="tim-icons icon-simple-remove"></i>
                            </button>
                        `
                    }
                ],
            })
            // console.log(data);
            // data.forEach(function(value, index){
            //     var row = `
            //     <tr>
            //         <td class="text-center data-id">`+value['id']+`</td>
            //         <td class="data-name">`+value['name']+`</td>
            //         <td class="data-username">`+value['username']+`</td>
            //         <td class="text-center">
                        // <button class="btn btn-success btn-fab btn-icon btn-round btn-sm edit" data-toggle="modal" data-target="#edit_modal" type="button">
                        //     <i class="tim-icons icon-pencil"></i>  
                        // </button>
                        // <button class="btn btn-success btn-fab btn-icon btn-round btn-sm delete" type="button">
                        //     <i class="tim-icons icon-simple-remove"></i>
                        // </button>
            //         </td>
            //     </tr>
            //     `
            //     $(".admins").append(row);
            // });
        }
            
    });
}
function tableInit(){
    $('#admins').DataTable({
        ajax: '/bioskop/public/api/admin/admin',
        columns: [
            {data: 'id'},
            {data: 'name'},
            {data: 'username'},
            {
                data: null,
                defaultContent: `
                    <button class="btn btn-success btn-fab btn-icon btn-round btn-sm edit" data-toggle="modal" data-target="#edit_modal" type="button">
                        <i class="tim-icons icon-pencil"></i>  
                    </button>
                    <button class="btn btn-success btn-fab btn-icon btn-round btn-sm delete" type="button">
                        <i class="tim-icons icon-simple-remove"></i>
                    </button>
                `
            }
        ]
    });
}
function reloadData(){
    $("#admins").DataTable().ajax.reload()
}

function empty_modal(){
    $("#add_name").val("");
    $("#add_username").val("");
    $("#add_password").val("");
    $("#add_cpassword").val("");
    $("#edit_name").val("");
    $("#edit_username").val("");
    $("#edit_old_password").val("");
    $("#edit_password").val("");
    $("#edit_cpassword").val("");
}
$(document).ready(function(){
    // get_data();
    tableInit()
    $('.dataTables_length').addClass('bs-select');
    $("#add_modal").modal("hide")
    $("#edit_modal").modal("hide")
    $(".admins").on('click', '.edit', function() {
        // $("#edit_name").val($(this).parent().siblings(".data-name").text());
        // $("#edit_id").val($(this).parent().siblings(".data-id").text());
        // $("#edit_username").val($(this).parent().siblings(".data-username").text());
        val = $("#admins").DataTable().row( $(this).parents('tr') ).data()
        $("#edit_id").val(val.id);
        $("#edit_name").val(val.name);
        $("#edit_username").val(val.username);
    });
    $(".add_admin").click(function() {
        $("#add_modal").modal("show");
    });
    $(".add_button").click(function(){
        if($("#add_password").val() != $("#add_cpassword").val()){
            $("#alert_text").val("password doesnt match");
            $("#alert").show();
        } else {
            $.ajax({
                method: "POST",
                url: "/bioskop/public/api/admin/add_admin",
                data:{
                    _token: _token,
                    name : $("#add_name").val(),
                    username : $("#add_username").val(),
                    password : $("#add_password").val()
                }
            }).done(function(data){
                $("#add_modal").modal("hide");
                if (data['err_code'] != 0){
                    $.notify({
                        message: data["msg"]
                    }, {
                        type: 'danger',
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        z_index: 9999
                    });
                }
                reloadData() 
                empty_modal();
            });
        }
    });
    $(".edit_button").click(function(){
        $.ajax({
            method: "POST",
            url: "/bioskop/public/api/admin/edit_admin",
            data:{
                _token: _token,
                id : $("edit_id").val(),
                name : $("#edit_name").val(),
                username : $("#edit_username").val(),
                old_password : $("#edit_old_password").val(),
                password : $("#edit_password").val(),
                cpassword : $("#edit_cpassword").val()
            }
        }).done(function(data){
            if (data['err_code'] != 0){
                    $.notify({
                        message: data["msg"]
                    }, {
                        type: 'danger',
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        z_index: 9999
                    });
            } 
            else {
                $("#edit_modal").modal("hide");
                    $.notify({
                        message: data["msg"]
                    }, {
                        type: 'success',
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        z_index: 9999
                    });
                reloadData() 
                empty_modal();
            }
            
        });
    });
    $(".admins").on('click', '.delete', function() {
        this_id = $("#admins").DataTable().row( $(this).parents('tr') ).data().id;
        $.confirm({
            title: 'Confirm!',
            content: 'Simple confirm!',
            theme: 'dark',
            type : 'dark',
            buttons: {
                confirm: function () {
                    $.ajax({
                        method: "POST",
                        url: "/bioskop/public/api/admin/delete_admin",
                        data:{
                            _token: _token,
                            id : this_id
                        }
                    }).done(function(data){
                        if (data['err_code'] != 0){
                            $.notify({
                                message: data["msg"]
                            }, {
                                type: 'danger',
                                animate: {
                                    enter: 'animated fadeInDown',
                                    exit: 'animated fadeOutUp'
                                },
                                z_index: 9999
                            });
                        } else {
                            $.notify({
                                message: data["msg"]
                            }, {
                                type: 'success',
                                animate: {
                                    enter: 'animated fadeInDown',
                                    exit: 'animated fadeOutUp'
                                },
                                z_index: 9999
                            });
                        }
                        reloadData();
                        empty_modal();
                    });
                },
                cancel: function () {
                },
                // somethingElse: {
                //     text: 'Something else',
                //     btnClass: 'btn-blue',
                //     keys: ['enter', 'shift'],
                //     action: function(){
                //         $.alert('Something else?');
                //     }
                // }
            }
        });
        
    });
});


</script>
@endsection

@section('admin')
    active
@endsection