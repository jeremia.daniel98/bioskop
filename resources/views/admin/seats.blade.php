@extends('layouts.admin_main')

@section('title')
    Show Status
@endsection 

@section('content')
    <div class="wrapper">
        <div class="section" style="padding-top: 0;">
            <div class="container" style="margin-left: 0;">
                <div class="row">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-6 bg-img">
                                <h1 class="title">{{ $film->title}}</h1>
                                <img style="box-shadow: 7px 7px 8px black" src="{{ asset("/img/film/$film->filename") }}" alt="">
                            </div>
                            <div class="col-6" style="margin-top: 8em;">
                                <h2 class="title">{{ $show->studio->cinema->city->name }}</h2>
                                <h3 class="title mb-1">{{ $show->studio->cinema->name }}</h3>
                                <h3 class="">{{ $show->studio->name }}</h3>
                                <h3 class="title mb-1">{{ $show->show_date }}</h3>
                                <h3 class="">{{ $show->start_time }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-6" id="seats">

                        {{-- <div class="container mt-5 animate__animated animate__fadeInUp">
                            <div class="card bg-dark" style="border: 0.5px solid white">
                                <div class="card-body">
                                    <p class="text-center">Screen</p>
                                </div>
                            </div>
                            <div class="section">
                                <div class="row justify-content-center">
                                    <div class="col-6">

                                    </div>
                                    <div class="col-6">
                                        
                                    </div>
                                    <div class="mb-3">
                                        <img style="height: 32px" src="{{ asset('/img/seat_empty.png') }}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div> --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection 

@section('script')
    <script>
        $(document).ready(function(){
            $.ajax({
                method: "GET",
                url: '/bioskop/public/api/studioid',
                data: {
                    _token: _token,
                    studioid: "{{ $show->studio_id }}",
                    showid: "{{ $show->id }}"
                }
            }).done(function(data){
                console.log(data)
                $("#seats").html('')
                $("#seats").append(`
                    <div class="container mt-5">
                        <div class="card bg-dark ml-3" style="border: 0.5px solid white">
                            <div class="card-body">
                                <p class="text-center">Screen</p>
                            </div>
                        </div>
                    </div>
                `)
                var app = '<div class="section">'
                var row_size = data['studio']['row_size']
                var col_size = data['studio']['col_size']
                var count = 0
                for (let i = 0; i < row_size; i++) {
                    app += '<div class="row justify-content-center">'
                    app += '<div class="col-1">'
                    app += '<div class="mb-3 ml-2 mr-2 mt-2" style="display: inline-block;">'+ String.fromCharCode('A'.charCodeAt(0) + i) + '</div></div>'
                    
                        for(let j = 0; j < col_size; j++){
                            if (data['seats'][count]['booking_id'] == null){
                                app += `
                                        <div class="mb-3 ml-2 mr-2" style="display: inline-block;">
                                            <img id="selectedseat" data-name=`+data['seats'][count]['number']+` data-seatid=`+data['seats'][count]['id']+` style="height: 32px" src="{{ asset('/img/seat_empty.png') }}" alt="">
                                        </div>
                                `
                            } else {
                                app += `
                                        <div class="mb-3 ml-2 mr-2" style="display: inline-block;" data-toggle="tooltip" data-placement="top" title="`+ data['seats'][count]['booking']['user']['name'] +`">
                                            <img id="selectedseat" data-name=`+data['seats'][count]['number']+` data-seatid=`+data['seats'][count]['id']+` style="height: 32px" src="{{ asset('/img/seat_full.png') }}" alt="">
                                        </div>
                                `
                            }
                            count += 1
                        }
                    app += '</div>'
                }

                app += '</div>'
                $("#seats").append(app)
            })
        })
    </script>
@endsection