@extends('layouts.admin_main')

@section('title')
    Bookings
@endsection 

@section('content')
    <!-- Main -->
    <div class="wrapper">
		<div class="section">
			<div class="container">
				<div class="row">
                    <div class="col" style="margin-top: 2em;">
						<h2>Bookings</h2>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<table class="table bookings" id="admins">
							<thead>
								<tr>
									<th class="text-center" style="width: 8.33%">Id</th>
									<th class="text-center">Film</th>
									<th class="text-center">Cinema</th>
									<th class="text-center">City</th>
                                    <th class="text-center">Show date</th>
									<th class="text-center">Show time</th>
                                    <th class="text-center">Username</th>
                                    <th class="text-center">Seats</th>
                                    <th class="text-center">Booking code</th>
									<th class="text-center" style="width: 5%">Action</th>
                                    
								</tr>
							</thead>
							<tbody class="table_data">

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </div>
@endsection 

@section('script')
<script>
function tableInit(){
    $('#admins').DataTable({
        ajax: '/bioskop/public/api/bookings',
        columns: [
            {data: 'id'},
            {data: 'show.film.title'},
            {data: 'show.studio.cinema.name'},
            {data: 'show.studio.cinema.city.name'},
            {data: 'show.show_date'},
            {data: 'show.start_time'},
            {data: 'user.username'},
            {
                render: function(data, type, row, meta){
                    var seats = ""
                    row.seats.forEach(function(value, index){
                        seats += value.number + " "
                    })
                    return seats
                }
            },
            {data: 'booking_code'},
            {
                render: function(data, type, row, meta){
                    return `
                    <button class="btn btn-success btn-fab btn-icon btn-round btn-sm delete" data-id="`+ row.id +`" type="button">
                        <i class="tim-icons icon-simple-remove"></i>
                    </button>
                    `
                }
            }
        ]
    });
}
function reloadData(){
    $("#admins").DataTable().ajax.reload()
}

function empty_modal(){
    $("#add_name").val("");
    $("#add_username").val("");
    $("#add_password").val("");
    $("#add_cpassword").val("");
    $("#edit_name").val("");
    $("#edit_username").val("");
    $("#edit_old_password").val("");
    $("#edit_password").val("");
    $("#edit_cpassword").val("");
}
$(document).ready(function(){
    tableInit()
    $(".bookings").on('click', '.delete', function() {
        this_id = $("#admins").DataTable().row( $(this).parents('tr') ).data().id;
        $.confirm({
            title: 'Are you sure?',
            content: 'This will refund the balance of the user if the show is active!',
            theme: 'dark',
            type : 'dark',
            buttons: {
                confirm: function () {
                    $.ajax({
                        method: "DELETE",
                        url: "/bioskop/public/api/booking/" + this_id,
                        data:{
                            _token: _token
                        },
                        success: function(data){
                            if (data['err_code'] != 0){
                                $.notify({
                                    message: data["msg"]
                                }, {
                                    type: 'danger',
                                    animate: {
                                        enter: 'animated fadeInDown',
                                        exit: 'animated fadeOutUp'
                                    },
                                    z_index: 9999
                                });
                            } else {
                                $.notify({
                                    message: data["msg"]
                                }, {
                                    type: 'success',
                                    animate: {
                                        enter: 'animated fadeInDown',
                                        exit: 'animated fadeOutUp'
                                    },
                                    z_index: 9999
                                });
                            }
                            reloadData();
                        },
                        error: function(){
                            $.notify({
                                    message: "Server Error"
                                }, {
                                    type: 'danger',
                                    animate: {
                                        enter: 'animated fadeInDown',
                                        exit: 'animated fadeOutUp'
                                    },
                                    z_index: 9999
                                });
                        }
                    })
                },
                cancel: function () {
                },
            }
        });
        
    });
});


</script>
@endsection

@section('booking')
    active
@endsection