<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>
            EDM•@yield('titlemain')
        </title>
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <!-- Nucleo Icons -->
        <link href="{{ asset('css/nucleo-icons.css') }}" rel="stylesheet" />
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">  
        <!-- Bootstrap Core CSS -->
        <!-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" /> -->
        <!-- Minimal Design Bootstrap -->
        <!-- <link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet" /> -->
        <!-- CSS Files -->
        <link href="{{ asset('css/blk-design-system.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
        <link href="{{ asset('css/addons/datatables.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" /> --}}
    
        @yield('style')
        
    </head>
    <body class="bg-dark">

        @yield('contentmain')
        
        <script src="{{ asset('/js/core/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/js/core/popper.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/js/core/bootstrap.min.js') }}" type="text/javascript"></script>
        <!-- <script src="{{ asset('/js/mdb.min.js') }}" type="text/javascript"></script> -->
        <script src="{{ asset('/js/notify.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/js/blk-design-system.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/js/plugins/bootstrap-switch.js') }}"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

        <!-- (Optional) Latest compiled and minified JavaScript translation files -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>
        <script>
            _token = $('meta[name="csrf-token"]').attr('content')
        </script>
        <script type="text/javascript" src="{{ asset('js/addons/datatables.min.js') }}"></script>
        {{-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script> --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

        @yield('scriptmain')

    </body>
</html>