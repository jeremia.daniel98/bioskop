@extends('layouts.main')

@section('titlemain')
    @yield('title')
@endsection

@section('style')
<style>
@media (max-width: 768px) {
    /* .nav-left{
        transition: all .1s linear;
    }
    .nav-left:hover .nav-label {
        display: inline-block;
    }
     */
    /* .apa {
        min-width: 1%;
    } */
    .bunderan {
        float:left;
    }
    .bunder {
        float:left;
    }
}
/* .nav-label {
        display: none;
        margin-left: 2em;
    } */
</style>
@endsection

@section('contentmain')
    <nav class="navbar navbar-expand-lg sticky-top bg-info">
        <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand" href="#">
                    <span>EDM•</span> Cinema
                </a>
                <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <div class="navbar-collapse-header">
                    <div class="row">
                      <div class="col-6 collapse-brand">
                        <a>
                            <span>EDM•</span> Cinema
                        </a>
                      </div>
                      <div class="col-6 collapse-close text-right">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                          <i class="tim-icons icon-simple-remove"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/bioskop/public/logout">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid" style="min-height: 100%;">
        <div class="row">
            <div class="col-2 bg-dark position-fixed apa" style="min-height: 100%;">
                <div class="nav-left mt-4">
                    <ul class="nav nav-pills nav-pills-info nav-justified flex-column">
                        <li class="nav-item">
                            <a class="nav-link @yield('film') d-none d-md-block" href="/bioskop/public/admin/film"><span class='fa fa-film'></span> <span class="nav-label">Films</span></a>
                            <a class="nav-link @yield('film') d-block d-md-none bunder" href="/bioskop/public/admin/film"><span class='bunderan fa fa-film'></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @yield('city') d-none d-md-block" href="/bioskop/public/admin/city"><span class='fa fa-university'></span> <span class="nav-label">Cities</span></a>
                            <a class="nav-link @yield('city') d-block d-md-none bunder" href="/bioskop/public/admin/city"><span class='bunderan fa fa-university'></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @yield('cinema') d-none d-md-block" href="/bioskop/public/admin/cinema"><span class='fa fa-building'></span> <span class="nav-label">Cinemas</span></a>
                            <a class="nav-link @yield('cinema') d-block d-md-none bunder" href="/bioskop/public/admin/cinema"><span class='bunderan fa fa-building'></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @yield('studio') d-none d-md-block" href="/bioskop/public/admin/studio"><span class='fa fa-desktop'></span> <span class="nav-label">Studios</span></a>
                            <a class="nav-link @yield('studio') d-block d-md-none bunder" href="/bioskop/public/admin/studio"><span class='bunderan fa fa-desktop'></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @yield('show') d-none d-md-block" href="/bioskop/public/admin/show"><span class='fa fa-server'></span> <span class="nav-label">Shows</span></a>
                            <a class="nav-link @yield('show') d-block d-md-none bunder" href="/bioskop/public/admin/show"><span class='bunderan fa fa-server'></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @yield('user') d-none d-md-block" href="/bioskop/public/admin/user"><span class='fa fa-user-circle'></span> <span class="nav-label">Users</span></a>
                            <a class="nav-link @yield('user') d-block d-md-none bunder" href="/bioskop/public/admin/user"><span class='bunderan fa fa-user-circle'></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @yield('admin') d-none d-md-block" href="/bioskop/public/admin/admin"><span class='fa fa-user-md'></span> <span class="nav-label">Admins</span></a>
                            <a class="nav-link @yield('admin') d-block d-md-none bunder" href="/bioskop/public/admin/admin"><span class='bunderan fa fa-user-md'></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @yield('voucher') d-none d-md-block" href="/bioskop/public/admin/voucher"><span class='fa fa-credit-card'></span> <span class="nav-label">Vouchers</span></a>
                            <a class="nav-link @yield('voucher') d-block d-md-none bunder" href="/bioskop/public/admin/voucher"><span class='bunderan fa fa-credit-card'></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @yield('booking') d-none d-md-block" href="/bioskop/public/admin/booking"><span class='fa fa-book'></span> <span class="nav-label">Bookings</span></a>
                            <a class="nav-link @yield('booking') d-block d-md-none bunder" href="/bioskop/public/admin/booking"><span class='bunderan fa fa-book'></span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-10 " style="margin-left: 16.67%; background: #25292D; min-height: 100%;">
                @yield('content')
            </div>
        </div>
    </div>

    
@endsection

@section('scriptmain')
    @yield('script')
@endsection