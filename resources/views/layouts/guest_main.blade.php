@extends('layouts.main')

@section('titlemain')
    @yield('title')
@endsection

@section('contentmain')
    <nav class="navbar navbar-expand-lg sticky-top navbar-transparent">
        <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand" href="landing">
                    <span>EDM•</span> Cinema
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            
            <div class="collapse navbar-collapse justify-content-end" id="navigation">
                <div class="navbar-collapse-header">
                    <div class="row">
                      <div class="col-6 collapse-brand">
                        <a>
                            <span>EDM•</span> Cinema
                        </a>
                      </div>
                      <div class="col-6 collapse-close text-right">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                          <i class="tim-icons icon-simple-remove"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="login">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="register">Register</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
@endsection
<script>
$(document).ready(function () {
    $(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $(".navbar-collapse").hasClass("navbar-collapse in");
        if (_opened === true && !clickover.hasClass("navbar-toggle")) {
            $("button.navbar-toggle").click();
        }
    });
});
</script>
@section('scriptmain')
    @yield('script')
@endsection