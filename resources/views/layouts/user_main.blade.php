@extends('layouts.main')

@section('titlemain')
    @yield('title')
@endsection

@section('style')
<style>
@media (max-width: 768px) {
    .bunderan {
        float:left;
    }
    .bunder {
        float:left;
    }
}
</style>
@endsection

@section('contentmain')
    <nav class="navbar navbar-expand-lg sticky-top bg-warning" style="">
        <div class="container-fluid">
            <div class="navbar-translate">
                <a class="navbar-brand" href="#">
                    <span>EDM•</span> Cinema
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navigation">
                <div class="collapse-close text-right ">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                          <i class="text-white tim-icons icon-simple-remove"></i>
                        </button>
                </div>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2">{{ $cur_user->name }} </span>
                            <i class="tim-icons icon-single-02"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="/bioskop/public/profile">Change Password</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/bioskop/public/logout">Logout</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <p class="title mt-2">Balance: {{ $cur_user->balance }}</p>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid" style="min-height: 100%;">
        <div class="row">
            <div class="col-2 bg-dark position-fixed" style="min-height: 100%;">
                <div class="nav-left mt-4">
                    <ul class="nav nav-pills nav-pills-warning flex-column">
                        <li class="nav-item">
                            <a class="nav-link @yield('show') d-none d-md-block" href="/bioskop/public/shows"><span class='fa fa-film'></span> <span class="nav-label">Now Playing</span></a>
                            <a class="nav-link @yield('show') d-block d-md-none bunder" href="/bioskop/public/shows"><span class='bunderan fa fa-film'></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @yield('topup') d-none d-md-block" href="/bioskop/public/topup"><span class='fa fa-credit-card'></span> <span class="nav-label">Top Up</span></a>
                            <a class="nav-link @yield('topup') d-block d-md-none bunder" href="/bioskop/public/topup"><span class='bunderan fa fa-credit-card'></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @yield('booking') d-none d-md-block" href="/bioskop/public/booking"><span class='fa fa-book'></span> <span class="nav-label">My Bookings</span></a>
                            <a class="nav-link @yield('booking') d-block d-md-none bunder" href="/bioskop/public/booking"><span class='bunderan fa fa-book'></span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-10" style="margin-left: 16.67%; background: #25292D; min-height: 100%;">
                @yield('content')
            </div>
        </div>
    </div>

    
@endsection

@section('scriptmain')
    @yield('script')
@endsection