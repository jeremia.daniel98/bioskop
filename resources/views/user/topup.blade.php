@extends('layouts.user_main')

@section('title')
    Top Up
@endsection 

@section('content')
    <div class="wrapper">
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2 class="title">Top Up</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="container">
                <div class="row justifiy-content-center">
                    <div class="col-md-4 col-sm-1"></div>
                    <div class="col-md-4 col-sm-8">
                        <div class="card bg-dark animate__animated animate__fadeInUp" style="border: 1px solid white">
                            <div class="card-body">
                                <form action="">
                                    <div class="form-group">
                                        <label class="text-white" for="code">Enter voucher code: </label>
                                        <input id="code" type="text" class="form-control" placeholder="Voucher code .....">
                                    </div>
                                    <button type="button" id="topup" class="btn btn-warning">Enter</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection 

@section('script')
    <script>
        $(document).ready(function() {
            $("#topup").click(function() {
                $.ajax({
                    method: 'POST',
                    url: '/bioskop/public/api/topup',
                    data: {
                        _token: _token,
                        code: $("#code").val(),
                        cur_user: {{ $cur_user->id }}
                    }
                }).done(function(data){
                    if(data['err_code'] == 0){
                        err = 'success'
                        window.location.href = window.location.href
                    }else{
                        err = 'danger'
                    }
                    $.notify({
                        message: data["msg"]
                    }, {
                        type: err,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        z_index: 9999
                    });
                    
                })
            })
        })
    </script>
@endsection

@section('topup')
    active
@endsection