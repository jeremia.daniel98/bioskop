@extends('layouts.user_main')

@section('title')
    Bookings
@endsection 

@section('content')
    <div class="wrapper">
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-4">
                        <h2 class="title">My Bookings</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="container">
                <h4 class="title animate__animated animate__fadeInUp">Active Bookings</h4>
                <div class="row" id="active_shows">


                    {{-- <div class="col-6 mb-4">
                        <div class="card bg-dark" style="box-shadow: 1px 1px 3px black; border: 0.5px solid white;">
                            <div class="card-header">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-3" style="padding-left: 0">
                                            <img class="card-image" src="{{ asset('img/film/endgame.jpg') }}" style="border-radius: 10px;">
                                        </div>
                                        <div class="col-9">
                                            <h2 class="card-title"><b>End Game</b></h2>
                                            <h4 class="card-title"><b>XXI Surabaya</b></h4>
                                            <h4 class="card-title">Tuesday, 23 May 2020, 13:50</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-4">
                                        <h4 class="card-text">Booking Code: </h4>
                                        <h4 class="card-text">Seats: </h4>
                                    </div>
                                    <div class="col-8">
                                        <h4 class="card-text"><b>CH7ED1</b></h4>
                                        <h4 class="card-text"><b>A1, A2</b></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-4">
                                        <p class="card-text">Regular Seat: </p>
                                        <p class="card-text">Total Price: </p>
                                    </div>
                                    <div class="col-8 text-right">
                                        <p class="card-text"><b>Rp 100.000 X2</b></p>
                                        <h4 class="card-text"><b>Rp 200.000</b></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                    


                </div>
            </div>
            <div class="container">
                <h4 class="title animate__animated animate__fadeInUp">Past Bookings</h4>
                <div class="row" id="past_shows">



                    {{-- <div class="col-6 mb-4">
                        <div class="card bg-dark" style="box-shadow: 1px 1px 3px black; border: 0.5px solid grey;">
                            <div class="card-header">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-2" style="padding-left: 0">
                                            <img class="card-image" src="{{ asset('img/film/endgame.jpg') }}" style="border-radius: 10px;">
                                        </div>
                                        <div class="col-10">
                                            <h3 class="card-title"><b>TITLE</b></h3>
                                            <h4 class="card-subtitle">Cinema</h4>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                TEST
                            </div>
                        </div>
                    </div> --}}



                </div>
            </div>
        </div>
    </div>
@endsection 

@section('script')
    <script>
        $(document).ready(function() {
            $.ajax({
                method: 'GET',
                url: '/bioskop/public/api/book',
                data: {
                    _token: _token,
                    cur_user: {{ $cur_user->id }}
                }
            }).done(function(data){
                console.log(data)
                $("#active_shows").html('')
                if(data['active'].length > 0){
                    data['active'].forEach(function(value, index){
                        var app = `
                        <div class="col-lg-6 col-xs-12 col-sm-12 mb-4 animate__animated animate__fadeInUp">
                            <div class="card bg-dark" style="box-shadow: 1px 1px 3px black; border: 0.5px solid white;">
                                <div class="card-header">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-3" style="padding-left: 0">
                                                <img class="card-image" src="{{ asset('img/film/`+ value[`seats`][0][`show`][`film`][`filename`] +`') }}" style="border-radius: 10px;">
                                            </div>
                                            <div class="col-9">
                                                <h2 class="card-title"><b>`+ value[`seats`][0][`show`][`film`][`title`] +`</b></h2>
                                                <h4 class="card-title"><b>`+ value[`seats`][0][`show`][`studio`][`cinema`][`name`] +`</b></h4>
                                                <h4 class="card-title text-muted"><b>`+ value[`seats`][0][`show`][`studio`][`cinema`][`city`][`name`] +`</b></h4>
                                                <h4 class="card-title">`+ value[`seats`][0][`show`][`show_date`] +` <b>`+ value[`seats`][0][`show`][`start_time`] +`</b></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                            <h4 class="card-text">Booking Code: </h4>
                                            <h4 class="card-text">Seats: </h4>
                                        </div>
                                        <div class="col-8">
                                            <h4 class="card-text"><b>`+ value[`booking_code`] +`</b></h4>
                                            <h4 class="card-text"><b>`
                                            value['seats'].forEach(function(val, ind){
                                                app += val[`number`] + ` `
                                            })
                                            app +=`</b></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-4">
                                            <p class="card-text">Regular Seat: </p>
                                            <p class="card-text">Total Price: </p>
                                        </div>
                                        <div class="col-8 text-right">
                                            <p class="card-text"><b>Rp `+ value[`seats`][0][`show`]['price'] +` X`+ value[`seats`].length +`</b></p>
                                            <h4 class="card-text"><b>Rp `+ value[`seats`][0][`show`]['price'] * value[`seats`].length +`</b></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `
                        $("#active_shows").append(app)
                    })
                }else {
                    $("#active_shows").append(`
                        <div class="col-lg-3 col-xs-12 col-sm-12 mb-4 animate__animated animate__fadeInUp">
                            <div class="card bg-dark animate__animated animate__fadeInUp" style="min-width: 100%; border: 0.5px solid white;">
                                <div class="card-header">
                                    <h3 class="card-title">You have no active bookings</h3>
                                </div>
                            </div>
                        </div>
                    `)
                }

                $("#past_shows").html('')
                if(data['past'].length > 0){
                    data['past'].forEach(function(value, index){
                        var app = `
                        <div class="col-lg-6 col-xs-12 col-sm-12 mb-4 animate__animated animate__fadeInUp">
                            <div class="card bg-dark" style="box-shadow: 1px 1px 3px black; border: 0.5px solid white;">
                                <div class="card-header">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-3" style="padding-left: 0">
                                                <img class="card-image" src="{{ asset('img/film/`+ value[`seats`][0][`show`][`film`][`filename`] +`') }}" style="border-radius: 10px;">
                                            </div>
                                            <div class="col-9">
                                                <h2 class="card-title"><b>`+ value[`seats`][0][`show`][`film`][`title`] +`</b></h2>
                                                <h4 class="card-title"><b>`+ value[`seats`][0][`show`][`studio`][`cinema`][`name`] +`</b></h4>
                                                <h4 class="card-title text-muted"><b>`+ value[`seats`][0][`show`][`studio`][`cinema`][`city`][`name`] +`</b></h4>
                                                <h4 class="card-title">`+ value[`seats`][0][`show`][`show_date`] +` <b>`+ value[`seats`][0][`show`][`start_time`] +`</b></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                            <h4 class="card-text">Booking Code: </h4>
                                            <h4 class="card-text">Seats: </h4>
                                        </div>
                                        <div class="col-8">
                                            <h4 class="card-text"><b>`+ value[`booking_code`] +`</b></h4>
                                            <h4 class="card-text"><b>`
                                            value['seats'].forEach(function(val, ind){
                                                app += val[`number`] + ` `
                                            })
                                            app +=`</b></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-4">
                                            <p class="card-text">Regular Seat: </p>
                                            <p class="card-text">Total Price: </p>
                                        </div>
                                        <div class="col-8 text-right">
                                            <p class="card-text"><b>Rp `+ value[`seats`][0][`show`]['price'] +` X`+ value[`seats`].length +`</b></p>
                                            <h4 class="card-text"><b>Rp `+ value[`seats`][0][`show`]['price'] * value[`seats`].length +`</b></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `
                        $("#past_shows").append(app)
                    })
                }else {
                    $("#past_shows").append(`
                        <div class="col-lg-3 col-xs-12 col-sm-12 mb-4 animate__animated animate__fadeInUp">
                            <div class="card bg-dark animate__animated animate__fadeInUp" style="min-width: 100%; border: 0.5px solid white;">
                                <div class="card-header">
                                    <h3 class="card-title">You have no past bookings</h3>
                                </div>
                            </div>
                        </div>
                    `)
                }
                
            })
        })
    </script>
@endsection

@section('booking')
    active
@endsection