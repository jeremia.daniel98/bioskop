@extends('layouts.user_main')

@section('title')
    {{ $cur_user->name }}
@endsection

@section('content')
    <div class="wrapper">
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-4">
                        <h2 class="title">Change Password</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="container">
                <div class="row justifiy-content-center">
                    <div class="col-4"></div>
                    <div class="col-4">
                        <div class="card bg-dark animate__animated animate__fadeInUp" style="border: 1px solid white">
                            <div class="card-body">
                                <form action="">
                                    <div class="form-group">
                                        <label class="text-white" for="code">Enter Old Password: </label>
                                        <input id="old" type="password" class="form-control" placeholder="">
                                    </div>

                                    <div class="form-group">
                                        <label class="text-white" for="code">Enter New Password: </label>
                                        <input id="new" type="password" class="form-control" placeholder="">
                                    </div>

                                    <div class="form-group">
                                        <label class="text-white" for="code">Confirm New Password: </label>
                                        <input id="renew" type="password" class="form-control" placeholder="">
                                    </div>
                                    <button type="button" id="change" class="btn btn-warning">Enter</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $("#change").click(function(){
                $new = $("#new").val()
                $renew = $("#renew").val()
                if ($new != $renew){
                    $.notify({
                        message: "New password and confirmation doesn't match"
                    }, {
                        type: 'danger',
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        z_index: 9999
                    });
                } else {
                    $.ajax({
                        url: '/bioskop/public/api/change/' + "{{ $cur_user->id }}",
                        method: 'POST',
                        data: {
                            _token: _token,
                            old: $("#old").val(),
                            new: $new
                        },
                        success: function(data){
                            if(data['err_code'] == 0){
                                err = 'success'
                            } else {
                                err = 'danger'
                            }
                            $.notify({
                                message: data['msg']
                            }, {
                                type: err,
                                animate: {
                                    enter: 'animated fadeInDown',
                                    exit: 'animated fadeOutUp'
                                },
                                z_index: 9999
                            });
                        },
                        error: function(){
                            $.notify({
                                message: "Server Error"
                            }, {
                                type: 'danger',
                                animate: {
                                    enter: 'animated fadeInDown',
                                    exit: 'animated fadeOutUp'
                                },
                                z_index: 9999
                            });
                        }
                    })
                }
            })
        })
    </script>
@endsection
