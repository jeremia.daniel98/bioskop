@extends('layouts.user_main')

@section('title')
    {{ $film->title }}
@endsection 

@section('content')
    <div class="wrapper">
        <div class="section" style="padding-top: 0;">
            <div class="container" style="margin-left: 0;">
                <div class="row">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-6 bg-img">
                                <h2 class="title">{{ $film->title }}</h2>
                                <img style="box-shadow: 7px 7px 8px black" src="{{ asset("/img/film/$film->filename") }}" alt="">
                            </div>
                            <div class="col-6 pt-5">
                                <div class="ml-3 animate__animated animate__fadeInUp" id="showlist">
                                    @foreach ($list as $i)
                                        <div class="row">
                                            <h3 class="ml-3" style="margin-bottom: 0;">{{ $i['cinema']['name'] }}</h3>
                                        </div>
                                        <div class="row">
                                            <h4 class="ml-3" style="margin-bottom: 5px;">Rp {{ $i['shows'][0]['price'] }}</h4>
                                        </div>
                                        <div class="row mb-4">
                                        @foreach ($i['shows'] as $s)
                                            <div class="col-auto">
                                                <button id="selectedshow" class="btn btn-warning btn-round btn-sm" data-price={{ $s['price'] }} data-time={{ $s['start_time'] }} data-showid={{ $s['id'] }} data-studioid={{ $s['studio_id'] }}>
                                                    {{ $s['start_time'] }}
                                                </button>
                                            </div>
                                        @endforeach
                                        </div>
                                    @endforeach
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-6" id="seats">

                        {{-- <div class="container mt-5 animate__animated animate__fadeInUp">
                            <div class="card bg-dark" style="border: 0.5px solid white">
                                <div class="card-body">
                                    <p class="text-center">Screen</p>
                                </div>
                            </div>
                            <div class="section">
                                <div class="row justify-content-center">
                                    <div class="col-6">

                                    </div>
                                    <div class="col-6">
                                        
                                    </div>
                                    <div class="mb-3">
                                        <img style="height: 32px" src="{{ asset('/img/seat_empty.png') }}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div> --}}

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Add -->
    <div class="modal fade checkout_modal" id="checkout_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-dark">
                <div class="modal-header">
                    <h2 class="mb-1" id="c_title">{{ $film->title }}</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-5" style="padding-left: 0px;">
                                <h3 class="mb-1" id="c_date">{{ $cur_date }}</h3>
                            </div>
                            <div class="col-7 text-right">
                                <h3 class="mb-1" id="c_time"></h3>
                            </div>
                        </div>
                        <div class="row">
                            <hr style="border-top: 1px solid grey; width: 100%;">
                        </div>
                        <div class="row" id="">
                            <h4 class="mr-1" style="display: inline-block">Selected Seats: </h4>
                            <div id="c_seats"></div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <h4>Price: </h4>
                            </div>
                            <div id="subtotal" class="col-6 text-right text-white">

                            </div>
                        </div>
                        <div class="row">
                            <hr style="border-top: 1px solid grey; width: 100%;">
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <h4><b>Total: </b></h4>
                            </div>
                            <div id="total" class="col-6 text-right text-white">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-warning buy text-right">Buy</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- End Modal Add -->

@endsection 

@section('script')
    <script>
        selectedseat = []
        selectedseatid = []
        selectedshow = -1
        $(document).ready(function() {
            $("#showlist").on('click', '#selectedshow', function() {
                $("#c_time").html($(this).data("time"))
                $("#subtotal").html('<p class="text-white">@ Rp '+$(this).data('price')+'</p>')
                $("#subtotal").data('price', $(this).data('price'))
                selectedseat = []
                selectedseatid = []
                selectedshow = $(this).data('showid')
                $.ajax({
                    method: "GET",
                    url: '/bioskop/public/api/studioid',
                    data: {
                        _token: _token,
                        studioid: $(this).data('studioid'),
                        showid: $(this).data('showid')
                    }
                }).done(function(data){
                    $("#seats").html('')
                    $("#seats").append(`
                        <div class="container mt-5 animate__animated animate__fadeInUp">
                            <div class="card bg-dark ml-3" style="border: 0.5px solid white">
                                <div class="card-body">
                                    <p class="text-center">Screen</p>
                                </div>
                            </div>
                        </div>
                    `)
                    var app = '<div class="section animate__animated animate__fadeInUp pt-3">'
                    var row_size = data['studio']['row_size']
                    var col_size = data['studio']['col_size']
                    var count = 0
                    for (let i = 0; i < row_size; i++) {
                        app += '<div class="row justify-content-center">'
                        app += '<div class="col-1">'
                        app += '<div class="mb-3 ml-2 mr-2 mt-2" style="display: inline-block;">'+ String.fromCharCode('A'.charCodeAt(0) + i) + '</div></div>'
                        
                            for(let j = 0; j < col_size; j++){
                                if (data['seats'][count]['booking_id'] == null){
                                    app += `
                                            <div class="mb-3 ml-2 mr-2" style="display: inline-block;">
                                                <img id="selectedseat" data-name=`+data['seats'][count]['number']+` data-seatid=`+data['seats'][count]['id']+` style="height: 32px" src="{{ asset('/img/seat_empty.png') }}" alt="">
                                            </div>
                                    `
                                } else {
                                    app += `
                                            <div class="mb-3 ml-2 mr-2" style="display: inline-block;">
                                                <img id="selectedseat" data-name=`+data['seats'][count]['number']+` data-seatid=`+data['seats'][count]['id']+` style="height: 32px" src="{{ asset('/img/seat_full.png') }}" alt="">
                                            </div>
                                    `
                                }
                                count += 1
                            }
                        app += '</div>'
                    }

                    app += '</div>'
                    app += `
                        <div class="row animate__animated animate__fadeInUp">
                            <div id="selectedlist" class="col-6">
                                <p style="display: inline-block">Seats: </p>
                            </div>
                            <div class="col-6 text-right">
                                <button id="checkout" class="btn btn-warning" data-toggle="modal" data-target="#checkout_modal">Checkout</button>
                            </div>
                        </div>
                    `
                    $("#seats").append(app)
                })
            })

            //Seat Click
            $("#seats").on('click', '#selectedseat', function(){
                if($(this).attr('src') == "{{ asset('/img/seat_empty.png') }}"){
                    $(this).attr('src', "{{ asset('/img/seat_selected.png') }}")
                    selectedseat.push($(this).data('name'))
                    selectedseatid.push($(this).data('seatid'))
                    $("#selectedlist").html('<p style="display: inline-block">Seats: </p>')
                    selectedseat.forEach(function(value, index) {
                        $("#selectedlist").append(`
                            <p style="display: inline-block">`+value+` </p>
                        `)
                    })
                } else if ($(this).attr('src') == "{{ asset('/img/seat_selected.png') }}"){
                    $(this).attr('src', "{{ asset('/img/seat_empty.png') }}")

                    var index = selectedseat.indexOf($(this).data('name'))
                    if(index > -1) {
                        selectedseat.splice(index, 1)
                    }

                    index = selectedseatid.indexOf($(this).data('seatid'))
                    if(index > -1) {
                        selectedseatid.splice(index, 1)
                    }

                    $("#selectedlist").html('<p style="display: inline-block">Seats: </p>')
                    selectedseat.forEach(function(value, indexx) {
                        $("#selectedlist").append(`
                            <p style="display: inline-block">`+value+` </p>
                        `)
                    })
                }
            })

            $("#seats").on('click', '#checkout', function(){
                $("#c_seats").html(' ')
                selectedseat.forEach(function(value, index) {
                        $("#c_seats").append(`
                            <p class="text-white" style="display: inline-block">`+value+` </p>
                        `)
                    })
                var count = selectedseat.length
                $("#total").html('<h3 class="text-white"><b>Rp '+ count * $("#subtotal").data('price') +'</b></h3>')
            })

            // Checkout BUtton click
            $(".buy").click(function() {
                $.ajax({
                    method: 'POST',
                    url: '/bioskop/public/api/book',
                    data: {
                        _token: _token,
                        cur_user: {{ $cur_user->id }},
                        showid: selectedshow,
                        seats: selectedseatid
                    }
                }).done(function(data){
                    if(data['err_code'] == 0){
                        window.location.href = "/bioskop/public/booking"
                        err = 'success'
                    }else{
                        err = 'danger'
                    }
                    $.notify({
                        message: data["msg"]
                    }, {
                        type: err,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        z_index: 9999
                    });
                    
                })
            })

        })

    </script>
@endsection
