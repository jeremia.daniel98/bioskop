@extends('layouts.user_main')

@section('title')
    Now Playing
@endsection 

@section('content')
    <div class="wrapper">
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <h2 class="title">Now Playing</h2>
                        <h3 class="subtitle">{{ $cur_date }}</h3>
                    </div>
                    <div class="col-lg-2 col-xs-12 col-sm-12 text-lg-right">
                        <h4 class="title" style="margin-top: 1em;">City: </h4>
                    </div>
                    <div class="col-lg-4 col-xs-12 col-sm-12 justify-content-right" style="padding-left: 0">
                        <form action="#">
                            <div class="form-group">
                                <select class="form-control selectpicker" data-style="btn-warning" title="Select city....." name="city" id="city">
                                    
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" style="padding-top: 0">
            <div class="container-fluid">
                <div class="row justify-content-left" id="films">
                    <div class="col-md-4 col-sm-8 mb-4">
                        <div class="card bg-dark animate__animated animate__fadeInUp" style="min-width: 100%; border: 0.5px solid white;">
                            <div class="card-header">
                                <h3 class="card-title">Please select a city</h3>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="col-3 mb-4">
                        <div class="card bg-dark animate__animated animate__fadeInUp" style="min-width: 100%; border: 0.5px solid white;">
                            <div class="card-header">
                                <h1 class="card-title">Title</h1>
                                <div class="row">
                                    <div class="col-6">
                                        <h5 class="card-subtitle text-muted">Genre</h5>
                                    </div>
                                    <div class="col-6">
                                        <h5 class="card-subtitle text-muted text-right">XX Minutes</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                            <img src="{{ asset('img/film/superman.jpg') }}" alt="">
                            </div>
                            <div class="card-footer">
                                <p class="card-text">Desc</p>
                                <p class="card-text">Rating: X</p>
                            </div>
                        </div>
                    </div> --}}
                    
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Select Seat-->
    <div class="modal fade" id="seatsModal" tabindex="-1" role="dialog" aria-labelledby="showsModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div style="border: 3px solid #bbbbbb" class="modal-content bg-dark">
                <div class="modal-header">
                    <h5 class="modal-title text-white" id="showsModalLabel">Booking</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="prevModal">Prev</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Model Select Seat -->

    <!-- Modal Select Show-->
    <div class="modal fade" id="showsModal" tabindex="-1" role="dialog" aria-labelledby="showsModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div style="border: 3px solid #bbbbbb" class="modal-content bg-dark">
                <div class="modal-header">
                    <h5 class="modal-title text-white" id="showsModalLabel">Booking</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-4">
                                <div class="row justify-content-center" id="filmdesc">
                                    
                                </div>
                            </div> 
                            <div class="col-8" id="showlist">
                                
                                
                                    

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Model Select Show -->

@endsection 

@section('script')
    <script>
        function getCities(){
            $.ajax({
                method: "GET",
                url: "api/city",
                data: {
                    _token: _token
                }
            }).done(function(data){
                $("#city").html('')
                data.forEach(function(value, index) {
                    var row = "<option>"+ value['name'] +"</option>"
                    $("#city").append(row)
                })
                $("#city").selectpicker('refresh')
            })
        }
        $(document).ready(function(){
            // Combobox Cities
            getCities();

            //Get Currently Now Playing Movies in each city
            $("#city").change(function() {
                $.ajax({
                    method: "GET",
                    url: "api/nowplaying",
                    data:{
                        _token: _token,
                        city: $("#city").val()
                    }
                }).done(function(data){
                    $("#films").html('')
                    if (data.length == 0){
                        $("#films").append(`
                            <div class="col-lg-6 col-xs-12 col-sm-12 mb-4">
                                <div class="card bg-dark animate__animated animate__fadeInUp" style="min-width: 100%; border: 0.5px solid white;">
                                    <div class="card-header">
                                        <h4 class="card-title">Sorry, there are currrently no movies playing in this city</h4>
                                    </div>
                                </div>
                            </div>
                        `)
                    } else {
                        
                        data.forEach(function(value, index){
                            var row = `
                            
                            <div class="col-lg-3 col-xs-12 col-sm-12 mb-4">
                                <a href="trans/`+value[`id`]+`/`+$("#city").val()+`" id="filmclick" data-filmid=`+ value['id'] +`>
                                    <div id="filmcard" class="card bg-dark animate__animated animate__fadeInUp" style="min-width: 100%; border: 0px solid white;">
                                        <div class="card-header">
                                            <h1 class="card-title">`+ value['title'] +`</h1>
                                            <div class="row">
                                                <div class="col-6">
                                                    <h5 class="card-subtitle text-muted">`+ value['genre'] +`</h5>
                                                </div>
                                                <div class="col-6">
                                                    <h5 class="card-subtitle text-muted text-right">`+ value['duration'] +` Minutes</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body text-center">
                                            <img style="max-height: 20em;" src="{{ asset('img/film/`+ value[`filename`] +`') }}" alt="">
                                        </div>
                                        <div class="card-footer">
                                            <p class="card-text">` + value['description'] + `</p>
                                            <p class="card-text" style="display: inline-block;">Rating: </p>
                                            <img style="" src="{{ asset('img/rating/rating`+ value[`rating`] +`.png') }}" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            `
                            $("#films").append(row)
                        })
                    }
                    
                })
            })

            // Choose a film
            // $("#films").on('click', '#filmclick', function() {
            //     $.ajax({
            //         method: "GET",
            //         url: "api/shows",
            //         data: {
            //             _token: _token,
            //             filmid: $(this).data('filmid'),
            //             city: $("#city").val()
            //         }
            //     }).done(function(data){
            //         $("#showlist").html('')
            //         data.forEach(function(value, index){
            //             $("#showlist").append(`
            //                 <div class="row">
            //                     <h4 class="ml-3" style="margin-bottom: 0;">`+ value['cinema']['name'] +`</h4>
            //                 </div>
            //             `)
            //             var row = `<div class="row">`
            //             var count = 0
            //             value['shows'].forEach(function(val, ind){
            //                 row += `
            //                     <div class="col-auto">
            //                         <button id="selectedshow" class="btn btn-warning btn-round btn-sm" data-showid=`+value['shows']['id']+`>
            //                             `+ value['shows'][count]['start_time'] +`
            //                         </button>
            //                     </div>
            //                 `
            //                 count += 1
            //             })
            //             row += `</div>`
            //             $("#showlist").append(row)
            //         })
            //     })
                
            //     $.ajax({
            //         method: "GET",
            //         url: 'api/filmid',
            //         data: {
            //             _token : _token,
            //             filmid: $(this).data('filmid')
            //         }
            //     }).done(function(data){
                    
            //         $("#filmdesc").html('')
            //         $("#filmdesc").append(`
            //             <h3 style="margin-bottom: 0">`+ data['title'] +`</h3>
            //             <div class="container">
            //                 <img src="{{ asset('/img/film/` + data[`filename`] + `') }}" alt="">
            //             </div>
            //         `)
            //     })




            //     $("#showsModal").modal()
            // })

            // Seat Modal
            $("#showsModal").on('click', '#selectedshow', function(){
                $("#showsModal").modal('hide')
                $("#seatsModal").modal('show')
                $("#seatsModal").trigger('focus')
            })
            $("#seatsModal").on('click', '#prevModal', function() {
                $("#seatsModal").modal('hide')
                $("#showsModal").modal('show')
                $("#showsModal").trigger('focus')
            })
            



            // Movie Hover
            $("#films").on('mouseover', '#filmcard', function() {
                $(this).css("border", "0.5px solid white")
            })
            $("#films").on('mouseleave', '#filmcard', function() {
                $(this).css("border", "0px solid white")
            })
        })
    </script>
@endsection

@section('show')
    active
@endsection

