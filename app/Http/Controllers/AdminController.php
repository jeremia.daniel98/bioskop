<?php

namespace App\Http\Controllers;

use App\Admin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use PDOException;

class AdminController extends Controller
{
    public function get_register_page(){
        return view('admin.register');
    }
    public function register_admin(Request $request){
        $res = [
            'err_code' => 0,
            'msg' => ''
        ];

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
            'name' => 'required'
        ]);

        if ($validator->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        $admins = Admin::where('username', $request->username)->get();
        $users = User::where('username', $request->username)->get();
        
        if (count($admins) > 0 || count($users) > 0){
            $res['err_code'] = 1;
            $res['msg'] = "Username Taken";
            return response()->json($res);
        }

        try {
            $admin = new Admin;
            $admin->name = $request->name;
            $admin->username = $request->username;
            $admin->password = Hash::make($request->password);
            $admin->save();
        } catch(PDOException $e){
            $res['err_code'] = 2;
            $res['msg'] = "Database Error";
            return response()->json($res);
        } 
        
        $res['msg'] = '/login';

        return response()->json($res);
    }
    public function getAdminsList(){

        $res = [
            'err_code' => 0,
            'msg' => ''
        ];

        $admins = Admin::all();
        $response = collect([
            'data' => $admins
        ]);
        return response()->json($response);
    }
    public function edit_admin(Request $request){
        $res = [
            'err_code' => 0,
            'msg' => 'Succesfully Updated!'
        ];

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
            'name' => 'required',
            'old_password' => 'required',
            'cpassword' => 'required',
        ]);

        if ($validator->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        if ($request->password != $request->cpassword){
            return [
                'err_code' => 6,
                'msg' => "Password doesn't match!"
            ];
        }
        try {
            $admins = Admin::where('username', $request->username)->get();
        } catch(PDOException $e){
            $res['err_code'] = 2;
            $res['msg'] = "Database Error";
            return response()->json($res);
        } 
        
        
        if (count($admins) == 0){
            $res['err_code'] = 1;
            $res['msg'] = "User doesn't exist!";
            return response()->json($res);
        }

        try {
            $admin = Admin::firstWhere("username", $request->username);
        } catch(PDOException $e){
            $res['err_code'] = 2;
            $res['msg'] = "Database Error";
            return response()->json($res);
        } 
        
        if (!Hash::check($request->old_password, $admin->password)){
            $res['err_code'] = 5;
            $res['msg'] = "Wrong password";
            return response()->json($res);
        }

        try {
            $admin->name = $request->name;
            if ($request->password != ""){
                $admin->password = Hash::make($request->password);
            }
            $admin->save();
        } catch(PDOException $e){
            $res['err_code'] = 2;
            $res['msg'] = "Database Error";
            return response()->json($res);
        } 
        return response()->json($res);
    }
    public function delete_admin(Request $request){
        $res = [
            'err_code' => 0,
            'msg' => 'Succesfully Deleted!'
        ];
        
        try {
            $deleted_admin = Admin::destroy($request->id);
        } catch(PDOException $e){
            $res['err_code'] = 2;
            $res['msg'] = "User not found";
        } 
        return response()->json($res);
    }
}
