<?php

namespace App\Http\Controllers;

use App\Cinema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDOException;

class CinemaController extends Controller
{
    public function get_by_city_id(Request $request){
        $res = [
            'err_code' => 0,
            'msg' => ''
        ];
        
        try {
            $cinemas = Cinema::whereHas('city', function($q) use ($request){
                $q->where("city_id", $request->id);
            })->get();
        } catch (PDOException $e){
            $res['err_code'] = 2;
            $res['msg'] = "Database Error";
            return response()->json($res);
        }
        return response()->json($cinemas);
    }

    public function getAllCinemas(Request $request){
        $response = ["data" => Cinema::with('city')->get()];
        return $response;
    }
    public function getByCinemaID(Request $request){
        return Cinema::find($request->id);
    }
    public function addCinemas(Request $request){
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'city' => 'required'
        ]);

        if ($validation->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        $cinema = new Cinema();
        $cinema->name = $request->name;
        $cinema->address = $request->address;
        $cinema->city_id = $request->city;
        $cinema->save();

        return [
            'err_code' => 0,
            'msg' => "Add Successful"
        ];
    }
    public function editCinemas(Request $request, $cinemaid){
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'city' => 'required'
        ]);

        if ($validation->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        $cinema = Cinema::find($cinemaid);
        $cinema->name = $request->name;
        $cinema->address = $request->address;
        $cinema->city_id = $request->city;
        $cinema->save();

        return [
            'err_code' => 0,
            'msg' => "Add Successful"
        ];
    }
    public function deleteCinemas(Request $request, $cinemaid){
        $cinema = Cinema::find($cinemaid);
        $cinema->delete();
        return [
            'err_code' => 0,
            'msg' => "Delete Successful"
        ];
    }
}
