<?php

namespace App\Http\Controllers;

use App\Admin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use PDOException;

class RegisterController extends Controller
{
    public function get_register_page(){
        return view('guest.register');
    }

    public function register(Request $request){

        $res = [
            'err_code' => 0,
            'msg' => ''
        ];

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
            'name' => 'required'
        ]);

        if ($validator->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        $admins = Admin::where('username', $request->username)->get();
        $users = User::where('username', $request->username)->get();

        if (count($admins) > 0 || count($users) > 0){
            $res['err_code'] = 1;
            $res['msg'] = "Username Taken";
            return response()->json($res);
        }

        try{
            $user = new User;
            $user->name = $request->name;
            $user->username = $request->username;
            $user->balance = 0;
            $user->password = Hash::make($request->password);
            $user->save();
        } catch(PDOException $e){
            $res['err_code'] = 2;
            $res['msg'] = "Database Error";
            return response()->json($res);
        }
        
        $res['msg'] = 'login';

        return response()->json($res);
    }
}
