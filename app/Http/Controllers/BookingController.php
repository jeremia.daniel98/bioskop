<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Cinema;
use App\Film;
use App\Seat;
use App\Show;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class BookingController extends Controller
{

    public function getMyBookingView(Request $request){
        return view('user.bookings', ["cur_user" => Auth::user(), "cur_date" => date('d F, Y')]);
    }

    public function getMyBooking(Request $request){
        $booking = Booking::with('seats.show.film', 'seats.show.studio.cinema.city')->where('user_id', $request->cur_user)->get();
        $response = collect([
            'active' => collect(),
            'past' => collect()
        ]);

        foreach($booking as $book){
            // Log::debug($book->id);
            // Log::debug($book->seats[0]->show->show_date);
            
            if($book->seats[0]->show->show_date == date('Y-m-d')){
                $response['active']->push($book);
            } else {
                $response['past']->push($book);
            }
            
        }

        // foreach($response['active'] as $active){
        //     $active->seats[0]->show->show_date = Carbon::parse($book->seats[0]->show->show_date)->format('l, d M, Y');
        //     $active->seats[0]->show->start_time = Carbon::parse($book->seats[0]->show->start_time)->format('H:i');
        // }
        // foreach($response['past'] as $active){
        //     $active->seats[0]->show->show_date = Carbon::parse($book->seats[0]->show->show_date)->format('l, d M, Y');
        //     $active->seats[0]->show->start_time = Carbon::parse($book->seats[0]->show->start_time)->format('H:i');
        // }
        return response()->json($response);
    }

    public function bookingPage(Request $request, $film_id, $city){
        $shows = Show::whereHas('film', function($q) use ($request, $film_id, $city){
            $q->where("id", $film_id);
        })->whereHas('studio', function($qu) use ($request, $film_id, $city) {
            $qu->whereHas('cinema', function($que) use ($request, $film_id, $city) {
                $que->whereHas('city', function($quer) use ($request, $film_id, $city) {
                    $quer->where('name', $city);
                });
            });
        })->where('show_date', date('Y-m-d'))->get();
        
        $cinema = Cinema::whereHas('city', function($q) use ($request, $film_id, $city) {
            $q->where('name', $city);
        })->whereHas('studios', function($qu) use ($request, $film_id, $city) {
            $qu->whereHas('shows', function($que) use ($request, $film_id, $city) {
                $que->whereHas('film', function($quer) use ($request, $film_id, $city) {
                    $quer->where('id', $film_id);
                })->where('show_date', date('Y-m-d'));
            });
        })->get();

        $response = collect();
        foreach($cinema as $cin){
            $sh = collect();
            foreach($shows as $show){
                foreach($cin->studios as $std){
                    if($std->id == $show->studio->id){
                        $show['start_time'] = date('H:i', strtotime($show['start_time']));
                        $sh->push($show);
                        break;
                    }
                }
            }
            $response->push(collect(["cinema" => $cin, "shows" => $sh]));
        }

        $film = Film::find($film_id);

        return view('user.transaction', [
            'list' => $response,
            'film' => $film,
            "cur_date" => date('d F, Y'),
            "cur_user" => Auth::user()
        ]);

    }

    public function bookShow(Request $request){
        $user = User::find($request->cur_user);
        $seats = $request->seats;
        $show = Show::find($request->showid);
        

        if (is_null($seats)){
            return response()->json([
                'err_code' => 1,
                'msg' => 'No seat selected'
            ]);
        }
        $price = count($seats) * $show->price;

        foreach ($seats as $s){
            $seat = Seat::find($s);
            if(!is_null($seat->booking_id)){
                return response()->json([
                    'err_code' => 2,
                    'msg' => 'One of the seat is already booked'
                ]);
            }
        }

        if ($price > $user->balance){
            return response()->json([
                'err_code' => 3,
                'msg' => 'User balance is not sufficient'
            ]);
        }
        

        $book = $user->bookings()->create([
            'show_id' => $show->id,
            'booking_code' => substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1, 6),
            'status' => 'ACTIVE',
            'payment_status' => 'PAID',
            'booking_date' => now()
        ]);

        foreach ($seats as $s){
            $seat = Seat::find($s);
            $seat->booking_id = $book->id;
            $seat->save();
        }
        $user->balance -= $price;
        $user->save();
        
        return response()->json([
            'err_code' => 0,
            'msg' => 'Seats have been booked'
        ]);
    }

    public function getAllBooking(Request $request){
        $bookings = Booking::with('user', 'seats', 'show.film', 'show.studio.cinema.city')->get();
        return ['data' => $bookings];
    }

    public function deleteBooking(Request $request, $bookid){
        $booking = Booking::find($bookid);
        $seats = $booking->seats;
        $user = $booking->user;
        $show = $booking->show;

        foreach($seats as $seat){
            $seat->booking_id = NULL;
            $seat->save();
        }

        if(strtotime($show->show_date . " " . $show->start_time) > time()){
            $user->balance += $show->price;
            $user->save();
            $booking->delete();
            return response()->json([
                'err_code' => 0,
                'msg' => 'Booking has been refunded! Seats are now available'
            ]);
        }

        
        $booking->delete();

        return response()->json([
            'err_code' => 0,
            'msg' => 'Booking deleted, but NOT refunded!'
        ]);

    }


}
