<?php

namespace App\Http\Controllers;

use App\Film;
use App\Show;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SeatController extends Controller
{
    public function showSeatsPage(Request $request, $showid){
        $show = Show::with('studio.cinema.city')->find($showid);
        $film = $show->film;
        Log::debug($film);
        return view('admin.seats', [
            'show' => $show,
            'film' => $film
        ]);
    }
}
