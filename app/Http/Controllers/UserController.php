<?php

namespace App\Http\Controllers;

use App\Admin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function getProfile(Request $request){
        return view('user.profile', ['cur_user' => Auth::user()]);
    }

    public function getAllUser(Request $request){
        return ['data' => User::all()];
    }
    public function addUser(Request $request){
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required',
            'password' => 'required'
        ]);

        if ($validation->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        $admins = Admin::where('username', $request->username)->get();
        $users = User::where('username', $request->username)->get();

        if (count($admins) > 0 || count($users) > 0){
            return [
                'err_code' => 1,
                'msg' => "Username Taken"
            ];
        }

        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->balance = 0;
        $user->save();
        return [
            'err_code' => 0,
            'msg' => "Add Successful"
        ];
    }
    public function editUser(Request $request, $userid){
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required',
            'balance' => 'required'
        ]);


        if ($validation->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        $numeric = Validator::make($request->all(), [
            'balance' => 'numeric'
        ]);

        if ($numeric->fails()){
            return [
                'err_code' => 5,
                'msg' => "Balance must be a numerical value"
            ];
        }
        $user = User::find($userid);
        if($user->username != $request->username){
            $admins = Admin::where('username', $request->username)->get();
            $users = User::where('username', $request->username)->get();
    
            if (count($admins) > 0 || count($users) > 0){
                return [
                    'err_code' => 1,
                    'msg' => "Username Taken"
                ];
            }
        }
        
        $user->name = $request->name;
        $user->username = $request->username;
        if($request->password != '')
            $user->password = Hash::make($request->password);
        $user->balance = $request->balance;
        $user->save();
        return [
            'err_code' => 0,
            'msg' => "Add Successful"
        ];
    }
    public function deleteUser(Request $request, $userid){
        $user = User::find($userid);

        $user->delete();
        return [
            'err_code' => 0,
            'msg' => "Delete Successful"
        ];

    }
    public function getUserById(Request $request){
        return User::find($request->id);
    }

    public function changePassword(Request $request, $userid){
        $validation = Validator::make($request->all(), [
            'old' => 'required',
            'new' => 'required'
        ]);


        if ($validation->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }


        $user = User::find($userid);
        if(!Hash::check($request->old, $user->password)){
            return [
                'err_code' => 1,
                'msg' => "Wrong Password"
            ];
        }

        $user->password = Hash::make($request->new);
        $user->save();
        
        return [
            'err_code' => 0,
            'msg' => "Password successfully changed"
        ];
    }
    
}
