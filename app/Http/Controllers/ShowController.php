<?php

namespace App\Http\Controllers;

use App\Cinema;
use App\City;
use App\Film;
use App\Show;
use App\Seat;
use App\Studio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use PDOException;


class ShowController extends Controller
{

    public function getShowView(Request $request){
        return view('user.shows', ["cur_user" => Auth::user(), "cur_date" => date('d F, Y')]);
    }

    public function getCurrentlyPlayingFilms(Request $request){
        $films = Film::whereHas('shows', function($query) use ($request){
            $query->whereHas('studio', function($q) use ($request){
                $q->whereHas('cinema', function($qu) use ($request){
                    $qu->whereHas('city' ,function($que) use ($request){
                        $que->where('name', $request->city);
                    });
                });
            })->where('show_date', date('Y-m-d'));
        })->get();
        return response()->json($films);
    }

    public function getAvailableShows(Request $request){
        $shows = Show::whereHas('film', function($q) use ($request){
            $q->where("id", $request->filmid);
        })->whereHas('studio', function($qu) use ($request) {
            $qu->whereHas('cinema', function($que) use ($request) {
                $que->whereHas('city', function($quer) use ($request) {
                    $quer->where('name', $request->city);
                });
            });
        })->where('show_date', date('Y-m-d'))->get();
        
        $cinema = Cinema::whereHas('city', function($q) use ($request) {
            $q->where('name', $request->city);
        })->whereHas('studios', function($qu) use ($request) {
            $qu->whereHas('shows', function($que) use ($request) {
                $que->whereHas('film', function($quer) use ($request) {
                    $quer->where('id', $request->filmid);
                })->where('show_date', date('Y-m-d'));
            });
        })->get();

        $response = collect();
        foreach($cinema as $cin){
            $sh = collect();
            foreach($shows as $show){
                foreach($cin->studios as $std){
                    if($std->id == $show->studio->id){
                        $show['start_time'] = date('H:i', strtotime($show['start_time']));
                        $sh->push($show);
                        break;
                    }
                }
            }
            $response->push(collect(["cinema" => $cin, "shows" => $sh]));
        }

        return response()->json($response);
    }

    public function getShowsList(Request $request){
        $response = collect();
        // $shows = Show::join('films', 'shows.film_id', '=', 'films.id')
        //     ->join('studios', 'shows.studio_id', '=', 'studios.id')
        //     ->join('cinemas', 'studios.cinema_id', '=', 'cinemas.id')
        //     ->join('cities', 'cinemas.city_id', '=', 'cities.id')
        //     ->select('shows.*','films.title as title', 'films.filename as cover', 'studios.name as studio', 'cinemas.name as cinema', 'cities.name as city')
        //     ->get();
        $shows = Show::with('film', 'studio.cinema.city')->get();
        //$shows = Show::all();
        $response = collect([
            'data' => $shows
        ]);
        return response()->json($response);
    }

    
    public function add_show(Request $request){
        $res = [
            'err_code' => 0,
            'msg' => 'Add Success!'
        ];

        
        $validator = Validator::make($request->all(), [
            'studio_id' => 'required',
            'film_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'weekday_price' => 'required'
        ]);
        // return response()->json($res);
        if ($validator->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }
        /*
        //change string to date, check availability
        // $from = date($request->start_date);
        // $to = date($request->end_date);

        // $start_date = Carbon::parse($request->start_date)->format('d M Y');
        // $end_date = Carbon::parse($request->end_date)->format('d M Y');
        
        // $period = CarbonPeriod::create($start_date, $end_date);

        // Reservation::whereBetween('reservation_from', [$from1, $to1])
        //     ->orWhereBetween('reservation_to', [$from2, $to2])
        //     ->whereNotBetween('reservation_to', [$from3, $to3])
        //     ->get();

        
            // $start = date("Y-m-d",strtotime($request->start_date));
            // $end = date("Y-m-d",strtotime($request->end_date."+1 day"));

            // $query= Show::whereBetween('show_date',[$start,$end])->get();
        // $check_show = Show::whereBetween('show_date', [$from, $to])->get();
        */

        //check if date valid
        // if (!Carbon::parse($request->start_date)->exists() || !Carbon::parse($request->start_date)->exists()){
        //     return [
        //         'err_code' => 4,
        //         'msg' => "Invalid Dates!"
        //     ];
        // }
        $from = new Carbon($request->start_date);
        $to = new Carbon($request->end_date);

        if (($from->diffInDays($to, false))<0){
            return [
                'err_code' => 4,
                'msg' => "Start date after end date!"
            ];
        }
        //$start_time = (new Carbon("08:30"))->format('H:i:s');
        $start_time = $request->start_time;
        // $start_time = new Carbon("07:30");
        // $start_time = Carbon::parse($start_time);
        $start_time = Carbon::createFromFormat('H:i', $start_time);
        $duration = Film::whereId($request->film_id)->first()->duration;
        $end_time = $start_time->copy();
        // $end_time = $start_time->format('H')*60 + $start_time->format('i')+$end_time;
        $end_time = $end_time->addMinutes($duration+30);
        // return $end_time->format('H:i');
        // return $end_time->format('H:i');
        // return $end_time;
        // return $end_time->format('H:i');
        // return Carbon::createFromTime($end_time/60, $end_time%60, 0, 0);
        // $start_time = Carbon::parse($start_time = (new Carbon("08:30"))->format("H:i:s"));
        $col = collect();
        for ($x = $from; $x <= $to; $x = $x->addDays(1)) {
            $show = Show::whereDate('show_date', '=', $x)->where('studio_id', '=', $request->studio_id)->get();
            $boleh = true;
            if(count($show)>0){
                $boleh = true;
                for ($y = 0; $y < count($show); $y++){
                    if ($start_time->between($show[$y]->start_time, $show[$y]->end_time)){
                        $col->push($show[$y]);
                        $boleh = false;
                    }
                    else if ($end_time->between($show[$y]->start_time, $show[$y]->end_time)){
                        $col->push($show[$y]);
                        $boleh = false;
                    } else {
                        $boleh = true;
                    }
                }
            } 
            if ($boleh == true){
                $new_show = new Show;
                $new_show->studio_id = $request->studio_id;
                $new_show->film_id = $request->film_id;
                $new_show->show_date = $x;
                $new_show->start_time = $start_time;
                $new_show->end_time = $end_time;
                if ($x->format('l') == 'Saturday' && $request->sat_price != ''){
                    $new_show->price = $request->sat_price;
                } else if ($x->format('l') == 'Sunday' && $request->sun_price != ''){
                    $new_show->price = $request->sun_price;
                } else {
                    $new_show->price = $request->weekday_price;
                }
                $new_show->save();

                //new seat
                $row = Studio::find($request->studio_id)->row_size;
                $cols = Studio::find($request->studio_id)->col_size;
                $char = 'A';
                for ($i=0;$i<$row;$i++){
                    for($j=1;$j<=$cols;$j++){
                        $seat = new Seat;
                        $seat->number = $char.$j;
                        $seat->booking_id = NULL;
                        $new_show->seats()->save($seat);
                    }
                    ++$char;
                }
            }
        }   
        
        if(count($col) > 0){
            return [
                'err_code' => 4,
                'msg' => '',
                'col' => $col
            ];
        } 
        return response()->json($res);
    }
    public function edit_show(Request $request){
        $res = [
            'err_code' => 0,
            'msg' => 'Edit Success!'
        ];

        $validator = Validator::make($request->all(), [
            'studio_id' => 'required',
            'film_id' => 'required',
            'date' => 'required',
            'id' => 'required',
            'start_time' => 'required',
            'price' => 'required'
        ]);
        // return response()->json($res);
        if ($validator->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }
        $start_time = $request->start_time;
        $start_time = Carbon::createFromFormat('H:i', $start_time);
        $duration = Film::whereId($request->film_id)->first()->duration;
        $end_time = $start_time->copy();
        $end_time = $end_time->addMinutes($duration+30);
        
        $date = new Carbon($request->date);
        $col = collect();
        for ($x = $date; $x <= $date; $x = $x->addDays(1)) {
            $show = Show::whereDate('show_date', '=', $x)->where('studio_id', '=', $request->studio_id)->get();
            $boleh = true;
            if(count($show)>0){
                $boleh = true;
                for ($y = 0; $y < count($show); $y++){
                    if ($start_time->between($show[$y]->start_time, $show[$y]->end_time)){
                        $col->push($show[$y]);
                        $boleh = false;
                    }
                    else if ($end_time->between($show[$y]->start_time, $show[$y]->end_time)){
                        $col->push($show[$y]);
                        $boleh = false;
                    } else {
                        $boleh = true;
                    }
                }
            } 
            $show = $show->first();
            $new_show = Show::find($request->id);
            if ($boleh == true || $show->id == $request->id){
                $new_show->studio_id = $request->studio_id;
                $new_show->film_id = $request->film_id;
                $new_show->show_date = $x;
                $new_show->start_time = $start_time;
                $new_show->end_time = $end_time;
                $new_show->price = $request->price;
                $new_show->save();
            break;
            }
        }   
        return response()->json($res);
    }
    public function delete_show(Request $request){
        $res = [
            'err_code' => 0,
            'msg' => 'Succesfully Deleted!'
        ];
        
        try {
            $deleted = Show::destroy($request->id);
        } catch(PDOException $e){
            $res['err_code'] = 2;
            $res['msg'] = "Show not found";
        } 
        return response()->json($res);
    }
}
