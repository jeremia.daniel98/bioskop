<?php

namespace App\Http\Controllers;

use App\Film;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDOException;

class FilmController extends Controller
{
    public function getFilmByID(Request $request){
        $film = Film::find($request->filmid);
        return response()->json($film);
    }
    public function getByShowId(Request $request){
        $res = [
            'err_code' => 0,
            'msg' => ''
        ];
        
        try {
            $films = Film::all();
        } catch (PDOException $e){
            $res['err_code'] = 2;
            $res['msg'] = "Database Error";
            return response()->json($res);
        }
        return response()->json($films);
    }

    public function getFilms(Request $request){
        $films = Film::all();
        $response = collect([
            "data" => $films
        ]);
        return response()->json($response);
    }

    public function addFilms(Request $request){
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'genre' => 'required',
            'duration' => 'required',
            'desc' => 'required',
            'rating' => 'required'
        ]);

        $filevalid = Validator::make($request->all(), [
            'filename' => 'required|image|mimes:jpeg,png,jpg,gif'
        ]);

        if ($validation->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        if ($filevalid->fails()){
            return [
                'err_code' => 5,
                'msg' => "File must be a .jpeg .png .jpg .gif files"
            ];
        }
        $file = $request->file('filename');
        $file->move(public_path('img/film'), $file->getClientOriginalName());

        $film = new Film();
        $film->title = $request->title;
        $film->genre = $request->genre;
        $film->duration = $request->duration;
        $film->rating = $request->rating;
        $film->description = $request->desc;
        $film->filename = $file->getClientOriginalName();
        $film->save();

        return response()->json(collect([
            'err_code' => 0,
            'msg' => "Add Successful"
        ]));
    }

    public function editFilm(Request $request, $filmid){
        $validation = Validator::make($request->all(), [
            'etitle' => 'required',
            'egenre' => 'required',
            'eduration' => 'required',
            'edesc' => 'required',
            'erating' => 'required'
        ]);

        if ($validation->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        $film = Film::find($filmid);
        $film->title = $request->etitle;
        $film->genre = $request->egenre;
        $film->duration = $request->eduration;
        $film->rating = $request->erating;
        $film->description = $request->edesc;

        if ($request->file('efilename')){
            $filevalid = Validator::make($request->all(), [
                'efilename' => 'required|image|mimes:jpeg,png,jpg,gif'
            ]);

            if ($filevalid->fails()){
                return [
                    'err_code' => 5,
                    'msg' => "File must be a .jpeg .png .jpg .gif files"
                ];
            }
            $file = $request->file('efilename');
            $file->move(public_path('img/film'), $file->getClientOriginalName());
            $film->filename = $file->getClientOriginalName();
        }

        $film->save();
        return response()->json(collect([
            'err_code' => 0,
            'msg' => "Edit Successful"
        ]));
    }

    public function deleteFilm(Request $request, $filmid){
        $film = Film::find($filmid);
        $film->delete();
        return response()->json(collect([
            'err_code' => 0,
            'msg' => "Delete Successful"
        ]));
    }
}
