<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    public function get(){
        return response()->json(City::all());
    }

    public function getCityByID(Request $request){
        return City::find($request->id);
    }

    public function getAllCities(){
        return response()->json(['data' => City::all()]);
    }

    public function addCity(Request $request){
        $validation = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validation->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        $city = new City();
        $city->name = $request->name;
        $city->save();
        return [
            'err_code' => 0,
            'msg' => "Add Successful"
        ];
    }

    public function editCity(Request $request, $cityid){
        $validation = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validation->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        $city = City::find($cityid);
        $city->name = $request->name;
        $city->save();
        return [
            'err_code' => 0,
            'msg' => "Add Successful"
        ];
    }

    public function deleteCity(Request $request, $cityid){
        $city = City::find($cityid);
        $city->delete();
        return response()->json(collect([
            'err_code' => 0,
            'msg' => "Delete Successful"
        ]));
    }
}
