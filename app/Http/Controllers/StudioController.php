<?php

namespace App\Http\Controllers;

use App\Show;
use App\Studio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDOException;

class StudioController extends Controller
{
    public function getStudioByShow(Request $request){
        $studio = Studio::find($request->studioid);
        $show = Show::find($request->showid);
        $response = collect([
            'studio' => $studio,
            'seats' => $show->seats()->with('booking.user')->get()
        ]);
        return response()->json($response);
    }

    public function getSeats(Request $request) {
        $studio = Studio::find($request->id);
        return response()->json($studio->seats);
    }

    public function getByCinemaId(Request $request){
        $res = [
            'err_code' => 0,
            'msg' => ''
        ];
        
        try {
            $cinemas = Studio::whereHas('cinema', function($q) use ($request){
                $q->where("cinema_id", $request->id);
            })->get();
        } catch (PDOException $e){
            $res['err_code'] = 2;
            $res['msg'] = "Database Error";
            return response()->json($res);
        }
        return response()->json($cinemas);
    }

    public function getAllStudios(Request $request){
        return ['data' => Studio::with('cinema.city')->get()];
    }
    public function addStudio(Request $request){
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'row' => 'required',
            'col' => 'required',
            'cinema' => 'required'
        ]);

        if ($validation->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        $studio = new Studio();
        $studio->name = $request->name;
        $studio->row_size = $request->row;
        $studio->col_size = $request->col;
        $studio->cinema_id = $request->cinema;
        $studio->save();
        return [
            'err_code' => 0,
            'msg' => "Add Successful"
        ];

    }
    public function editStudio(Request $request, $studioid){
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'row' => 'required',
            'col' => 'required',
            'cinema' => 'required'
        ]);

        if ($validation->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        $studio = Studio::find($studioid);
        $studio->name = $request->name;
        $studio->row_size = $request->row;
        $studio->col_size = $request->col;
        $studio->cinema_id = $request->cinema;
        $studio->save();
        return [
            'err_code' => 0,
            'msg' => "Edit Successful"
        ];
    }
    public function deleteStudio(Request $request, $studioid){
        $studio = Studio::find($studioid);

        $studio->delete();
        return [
            'err_code' => 0,
            'msg' => "Delete Successful"
        ];
        
    }
    public function getStudioByID(Request $request){
        return Studio::with('cinema.city')->where('id', $request->id)->first();
    }
}
