<?php

namespace App\Http\Controllers;

use App\User;
use App\Voucher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TopupController extends Controller
{
    public function getView(Request $request){
        return view('user.topup', ["cur_user" => Auth::user()]);
    }

    public function getAllVoucher(Request $request){
        return ["data" => Voucher::all()];
    }

    public function addVoucher(Request $request){
        $count = $request->count;
        for ($i=0; $i < $count; $i++) { 
            $v = new Voucher();
            $v->code = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyz'),1, 6);
            $v->value = $request->value;
            $v->user_id = NULL;
            $v->save();
        }
        return [
            'err_code' => 0,
            'msg' => "Voucher added succesfully"
        ];
    }

    public function topUp(Request $request){

        $validation = Validator::make($request->all(), [
            'code' => 'required',
        ]);

        if ($validation->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        $code = Voucher::where('code', $request->code)->first();
        if (!is_null($code)){
            if(is_null($code->user_id)){
                $user = User::find($request->cur_user);
                $user->balance += $code->value;
                $code->user_id = $user->id;
                $user->save();
                $code->save();
                return response()->json([
                    'err_code' => '0',
                    'msg' => 'Balance has been updated'
                ]);
            }else {
                return response()->json([
                    'err_code' => '1',
                    'msg' => 'Sorry, voucher code has been used'
                ]);
            }
            
        } else{
            return response()->json([
                'err_code' => '1',
                'msg' => 'Sorry, voucher code doesnt exist'
            ]);
        }
    }
}
