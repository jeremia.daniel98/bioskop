<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function get_login_page(Request $request){
        return view('guest.login');
    }

    public function login(Request $request){
        $validation = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);

        if ($validation->fails()){
            return [
                'err_code' => 4,
                'msg' => "Please fill all the required fields"
            ];
        }

        $cred = $request->only(['username', 'password']);
        
        if (Auth::guard('admin')->attempt($cred)){
            return [
                'err_code' => 0,
                'msg' => "admin"
            ];
        } else if (Auth::guard('user')->attempt($cred)){
            return [
                'err_code' => 0,
                'msg' => ""
            ];
        }

        return [
            'err_code' => 1,
            'msg' => "Wrong username and/or password"
        ];
    }

    public function logout(){
        if (Auth::guard('admin')->check()){
            Auth::guard('admin')->logout();
        } else if (Auth::guard('user')->check()){
            Auth::guard('user')->logout();
        }

        return redirect('/login');
    }
}
