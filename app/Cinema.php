<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cinema extends Model
{
    public function studios(){
        return $this->hasMany('App\Studio');
    }

    public function city(){
        return $this->belongsTo('App\City');
    }
    
}
