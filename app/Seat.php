<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seat extends Model
{
    public function booking(){
        return $this->belongsTo('App\Booking');
    }

    public function show(){
        return $this->belongsTo('App\Show');
    }
}
