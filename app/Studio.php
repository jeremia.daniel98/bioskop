<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Studio extends Model
{
    public function shows(){
        return $this->hasMany('App\Show');
    }

    public function cinema(){
        return $this->belongsTo('App\Cinema');
    }
}
