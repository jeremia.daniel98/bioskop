<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    protected $table = "cities";

    public function cinemas(){
        return $this->hasMany('App\Cinema');
    }
}
