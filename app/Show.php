<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Show extends Model
{
    public function bookings(){
        return $this->hasMany('App\Booking');
    }

    public function seats(){
        return $this->hasMany('App\Seat');
    }

    public function film(){
        return $this->belongsTo('App\Film');
    }

    public function studio(){
        return $this->belongsTo('App\Studio');
    }
}
