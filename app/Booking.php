<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }
    
    public function seats(){
        return $this->hasMany('App\Seat');
    }

    public function show(){
        return $this->belongsTo('App\Show');
    }

    protected $dates = [
        'booking_date',
    ];

    protected $fillable = [
        'show_id',
        'user_id',
        'booking_code',
        'status',
        'payment_status',
        'booking_date'
    ];
}
