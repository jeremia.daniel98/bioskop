<?php

use App\Admin;
use App\Booking;
use App\Cinema;
use App\City;
use App\Film;
use App\Show;
use App\Studio;
use App\Seat;
use App\User;
use App\Voucher;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth:user')->group(function() {
    Route::get('/', function () {
        return redirect('/shows');
    });

    Route::get('/shows', 'ShowController@getShowView');
    Route::get('/booking', 'BookingController@getMyBookingView');
    Route::get('/topup', 'TopupController@getView');
    Route::get('/trans/{film_id}/{city}', 'BookingController@bookingPage');
    Route::get('/profile', 'UserController@getProfile');
});

Route::middleware('auth:admin')->group(function() {
    Route::get('/admin', function() {
        return redirect('/admin/film');
    });
    Route::get('/admin/film', function() {
        return view('admin.films');
    });
    Route::get('/admin/city', function() {
        return view('admin.cities');
    });
    Route::get('/admin/cinema', function() {
        return view('admin.cinemas');
    });
    Route::get('/admin/studio', function() {
        return view('admin.studios');
    });
    Route::get('/admin/show', function() {
        return view('admin.shows');
    });
    Route::get('/admin/user', function() {
        return view('admin.users');
    });
    Route::get('/admin/voucher', function() {
        return view('admin.vouchers');
    });
    Route::get('/admin/booking', function() {
        return view('admin.bookings');
    });
    Route::get('/admin/seats/{showid}', 'SeatController@showSeatsPage');
    Route::get('/admin/admin', 'AdminController@get_register_page');
    Route::post('/admin/admin', 'AdminController@register_admin');
});




Route::middleware('guest')->group(function() {
    Route::get('/login', 'LoginController@get_login_page')->name('login');
    Route::post('/login', 'LoginController@login');
    
    Route::get('/register', 'RegisterController@get_register_page');
    Route::post('/register', 'RegisterController@register');
});

Route::get('/logout', 'LoginController@logout');



Route::get('/landing', function() {
    return view('guest.landing');
})->name('landing');

Route::get('/test', function() {

    return view('guest.test');
    
});