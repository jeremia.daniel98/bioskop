<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Shows
Route::get('/nowplaying', 'ShowController@getCurrentlyPlayingFilms');
Route::get('/shows', 'ShowController@getAvailableShows');
Route::get('/admin/shows', 'ShowController@getShowsList');
Route::post('/admin/add_show', 'ShowController@add_show');
Route::post('/admin/edit_show', 'ShowController@edit_show');
Route::post('/admin/delete_show', 'ShowController@delete_show');

//Bookings
Route::get('/bookings', 'BookingController@getAllBooking');
Route::delete('/booking/{bookingid}', 'BookingController@deleteBooking');


//User
Route::get('/user', 'UserController@getAllUser');
Route::get('/userid', 'UserController@getUserById');
Route::post('/user', 'UserController@addUser');
Route::post('/user/{userid}', 'UserController@editUser');
Route::delete('/user/{userid}', 'UserController@deleteUser');
Route::post('/change/{userid}', 'UserController@changePassword');

//Admins
Route::get('/admin/admin', 'AdminController@getAdminsList');
Route::post('/admin/add_admin', 'AdminController@register_admin');
Route::post('/admin/edit_admin', 'AdminController@edit_admin');
Route::post('/admin/delete_admin', 'AdminController@delete_admin');

//City
Route::get('/city', 'CityController@get');
Route::get('/cityid', 'CityController@getCityByID');
Route::get('/cities', 'CityController@getAllCities');
Route::post('/city', 'CityController@addCity');
Route::post('/city/{cityid}', 'CityController@editCity');
Route::delete('/city/{cityid}', 'CityController@deleteCity');

//Cinema
Route::post('/admin/cinema', 'CinemaController@get_by_city_id');
Route::get('/cinema', 'CinemaController@getAllCinemas');
Route::get('/cinemaid', 'CinemaController@getByCinemaID');
Route::post('/cinema', 'CinemaController@addCinemas');
Route::post('/cinema/{cinemaid}', 'CinemaController@editCinemas');
Route::delete('/cinema/{cinemaid}', 'CinemaController@deleteCinemas');

//Film
Route::get('/filmid', 'FilmController@getFilmByID');
Route::get('/admin/film', 'FilmController@getByShowId');
Route::get('/films', 'FilmController@getFilms');
Route::post('/film', 'FilmController@addFilms');
Route::post('/film/{filmid}', 'FilmController@editFilm');
Route::delete('/film/{filmid}', 'FilmController@deleteFilm');

//Show
Route::post('/admin/add_show', 'ShowController@add_show');
Route::post('/admin/edit_show', 'ShowController@edit_show');
Route::post('/admin/delete_show', 'ShowController@delete_show');

//Studio
Route::get('/studioid', 'StudioController@getStudioByShow');
Route::get('/seats', 'StudioController@getSeats');
Route::post('/admin/studio', 'StudioController@getByCinemaId');
Route::get('/studio', 'StudioController@getAllStudios');
Route::get('/studiobyid', 'StudioController@getStudioByID');
Route::post('/studio', 'StudioController@addStudio');
Route::post('/studio/{studioid}', 'StudioController@editStudio');
Route::delete('/studio/{studioid}', 'StudioController@deleteStudio');

//TopUp
Route::post('/topup', 'TopupController@topUp');
Route::get('/voucher', 'TopupController@getAllVoucher');
Route::post('/voucher', 'TopupController@addVoucher');

//Book
Route::post('/book', 'BookingController@bookShow');
Route::get('/book', 'BookingController@getMyBooking');
Route::post('/admin/add_booking', 'BookingController@add_booking');
Route::post('/admin/edit_booking', 'BookingController@edit_booking');
Route::post('/admin/delete_booking', 'BookingController@delete_booking');